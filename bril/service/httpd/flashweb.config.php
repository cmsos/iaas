<?php
	// cluster of elasticsearch daemons the same as in elasticsearch.yml file
	$config ['hosts'] = array (
			"srv-s2d16-22-01"
	);

	$config ['elasticsearchurl'] = 'http://cmsos-iaas-bril.cms:9200';
	$config ['maxsources'] = 5000;
	$config ['timestreamsignature'] = 'urn:cmsos:xdaq-timestream';
	$config ['metaspheresignature'] = 'urn:cmsos:metasphere';
?>
