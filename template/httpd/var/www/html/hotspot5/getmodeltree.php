<?php
require '../utils/vendor/autoload.php';

include_once ('config.php');

$_arcurl = $_GET ['arcurl'];

$response = Network::httpget ( $_arcurl . "/getModelTree" );

$json = json_decode ( $response->getBody (), true );

foreach ( $json as $value ) {
	if ($value ['id'] == "1") {
		// fill trash node as root
		$trash ['id'] = "-1";
		$trash ['key'] = "-1";
		$trash ['title'] = "Trash";
		$trash ['lastupdate'] = $value ['lastupdate'];
		$trash ['raisedunaccepted'] = $value ['raisedunaccepted'];
		$trash ['raisedaccepted'] = $value ['raisedaccepted'];
		$trash ['clearedunaccepted'] = $value ['clearedunaccepted'];
		$trash ['clearedaccepted'] = $value ['clearedaccepted'];
		$trash ['consipicuity'] = "unknown";
		$trash ['tags'] = array ();
		$tags = intval ( str_replace ( " ", "", $value ['clearedunaccepted'] ) ) + intval ( str_replace ( " ", "", $value ['clearedaccepted'] ) );
		// force in tags counter sum of cleared exceptions
		array_push ( $trash ['tags'], $tags );
		// add trash node to model tree for homogenous display
		array_push ( $json, $trash );
		break;
	}
}

$response = json_encode ( $json );

header ( 'Content-Type: application/json; charset=utf-8' );

echo $response;
?>
