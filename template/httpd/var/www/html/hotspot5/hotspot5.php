<!DOCTYPE html>
<html lang="en">
<head>
<title>Hotspot5</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- jQuery -->
<script src="../utils/js/jquery-3.2.1.min.js"></script>

<!-- Bootstrap -->
<link rel="stylesheet" href="../utils/bootstrap-3.3.7-dist/css/bootstrap.min.css">
<script src="../utils/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>

<!-- DataTables -->
<link rel="stylesheet" href="../utils/DataTables-1.10.15/media/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="../utils/DataTables-1.10.15/extensions/Select/css/select.dataTables.min.css">
<link rel="stylesheet" href="../utils/DataTables-1.10.15/extensions/Buttons/css/buttons.dataTables.min.css">
<script src="../utils/DataTables-1.10.15/media/js/jquery.dataTables.min.js"></script>
<script src="../utils/DataTables-1.10.15/extensions/Select/js/dataTables.select.min.js"></script>
<script src="../utils/DataTables-1.10.15/extensions/Buttons/js/dataTables.buttons.min.js"></script>
<script src="../utils/Plugins-1.10.15/dataRender/ellipsis.js"></script>

<!-- Fancytree -->
<link rel="stylesheet" href="../utils/fancytree-2.24.0/dist/skin-bootstrap-n/ui.fancytree.min.css">
<script src="../utils/fancytree-2.24.0/dist/jquery.fancytree-all-deps.min.js"></script>
<script src="../utils/fancytree-2.24.0/dist/src/jquery.fancytree.table.js"></script>

<link href="../utils/css/pulse.css" rel="stylesheet" type="text/css">
<style>
.navbar{
	margin-bottom:2px;
}
.navbar-nav > li > a, .navbar-brand {
	padding-top:5px !important; padding-bottom:0px !important;
	height: 30px;
}
.navbar {min-height:30px !important;}

/* Define custom width and alignment of table columns */
#treetable {
	table-layout: fixed;
}

#treetable tr td:nth-of-type(1) {
	min-width: 100px;
	white-space: nowrap;
	overflow: hidden;
	text-overflow: ellipsis;
}

#treetable tr td:nth-of-type(2) {
	text-align: right;
}

/* Set gray background color and 100% height */
.sidenav {
	height: 100%;
	background-color: SlateGray ;
	overflow-y: scroll;
	
	z-index: 1; /* Stay on top */
	top: 0;
	left: 0;
	overflow-x: hidden; /* Disable horizontal scroll */
	padding-top: 4px; /* Place content 60px from the top */
	transition: 0.5s; /* 0.5 second transition effect to slide in the sidenav */
}

.rightnav {
	overflow-y: hidden;
	overflow-x: hidden; /* Disable horizontal scroll */

}


.fullview {
	height: 100%;
}

html, body {
	height: 100%;
}

.container-fluid {
	height: 100%;
	overflow-y: scroll; /* don't show content that exceeds my height */
}

/* Set black background color, white text and some padding */
/*footer {
	background-color: #555;
	color: white;
	padding: 5px;
	width: 100%;
}*/

#buttonbar {
	background-color: #555;
	color: white;
	padding: 15px;
}


.RaisedUnaccepted {
	font-weight: bold;
}
.RaisedAccepted {
	font-weight: normal;
}

.severity-warning {
	font-size: 200%;
	padding:0!important;
	color: gold;
	text-align: center;
}

.severity-fatal {
	font-size: 200%;
	padding:0!important;
	text-align: center;
	color: red;
}

.severity-error {
	font-size: 200%;
	padding:0!important;
	text-align: center;
	color: DarkOrange;
}

.severity-unknown {
	font-size: 200%;
	padding:0!important;
	text-align: center;
	color: grey;
}

span.conspicuity-warning {
	background-color: gold;
}

span.conspicuity-fatal {
	background-color: red;
}

span.conspicuity-error {
	background-color: DarkOrange;
}

.table-borderless > tbody > tr > td,
.table-borderless > tbody > tr > th,
.table-borderless > tfoot > tr > td,
.table-borderless > tfoot > tr > th,
.table-borderless > thead > tr > td,
.table-borderless > thead > tr > th {
	border: none!important;
}

td.highlight {
	font-weight: bold;
}

#exceptionview > ul {
	border: none;
}
.floatRight{
	float:right;
}

#autorefresh {
	margin-top: 5px;
	margin-bottom: 5px;
}
</style>
<script>

var severities = ["fatal", "error", "warning"];

var zone = "<?php echo $_GET['zone']?>";
var arcurl = "<?php echo $_GET['arcurl']?>";

glyph_opts_model = {
	preset: "bootstrap3",
	map: {
		doc: "glyphicon glyphicon-file",
		docOpen: "glyphicon glyphicon-file",
		checkbox: "glyphicon glyphicon-unchecked",
		checkboxSelected: "glyphicon glyphicon-check",
		checkboxUnknown: "glyphicon glyphicon-share",
		dragHelper: "glyphicon glyphicon-play",
		dropMarker: "glyphicon glyphicon-arrow-right",
		error: "glyphicon glyphicon-warning-sign",
		expanderClosed: "glyphicon glyphicon-menu-right",
		expanderLazy: "glyphicon glyphicon-menu-right", // glyphicon-plus-sign
		expanderOpen: "glyphicon glyphicon-menu-down",  // glyphicon-collapse-down
		folder: "glyphicon glyphicon-folder-close",
		folderOpen: "glyphicon glyphicon-folder-open",
		loading: "glyphicon glyphicon-refresh glyphicon-spin"
	}
};

glyph_opts_exception = {
	preset: "bootstrap3",
	map: {
		doc: "glyphicon",
		docOpen: "glyphicon",
		checkbox: "glyphicon glyphicon-unchecked",
		checkboxSelected: "glyphicon glyphicon-check",
		checkboxUnknown: "glyphicon glyphicon-share",
		dragHelper: "glyphicon glyphicon-play",
		dropMarker: "glyphicon glyphicon-arrow-right",
		error: "glyphicon glyphicon-warning-sign",
		expanderClosed: "glyphicon glyphicon-menu-right",
		expanderLazy: "glyphicon glyphicon-menu-right", // glyphicon-plus-sign
		expanderOpen: "glyphicon glyphicon-menu-down",  // glyphicon-collapse-down
		folder: "glyphicon",
		folderOpen: "glyphiconn",
		loading: "glyphicon glyphicon-refresh glyphicon-spin"
	}
};

function selectExceptionRow(uuid)
{
	var newSourceOption = {
		url: 'getexception.php?arcurl=' + arcurl,
		type: 'GET',
		data: {
			id: uuid
		},
		dataType: 'json'
	};

	var tree = $('#exceptionview').fancytree('getTree');
	tree.reload(newSourceOption);
}

function doNodeSelection(nodeid) {
	var exceptionTable = $("#exceptiontable").dataTable().api();

	//Clearing eceptionproperties
	var exceptionproperties = $("#exceptionproperties").dataTable().api();
	exceptionproperties.clear();
	exceptionproperties.draw();

	//Clearing exceptiontreeview
	var tree = $('#exceptionview').fancytree('getTree');
	tree.reload([]);
	
	// check if Trash
	if ( nodeid == '-1' ) {
		// then retrieve root node with state cleared
		exceptionTable.ajax.url('getexceptionlist.php?arcurl=' + arcurl + '&nodeid=' + '1' + '&state=ClearedUnaccepted|ClearedAccepted').load( null, true );
	}
	else {
		exceptionTable.ajax.url('getexceptionlist.php?arcurl=' + arcurl + '&nodeid=' + nodeid + '&state=RaisedUnaccepted|RaisedAccepted').load( null, true );
	}
}

function importTree(modelTree, tree) {
	for (var key in modelTree) {
		//update node
		var node = tree.getNodeByKey(modelTree[key].key);
		node.data.tags[0] = modelTree[key].tags[0];
		node.data.lastupdate = modelTree[key].lastupdate;
		node.data.conspicuity = modelTree[key].conspicuity;
		importTree(modelTree[key].children, tree);
	}
}

function refreshModelTree(data) {
	modelTree = data;

	var tree = $('#treetable').fancytree('getTree');
	var node = tree.getActiveNode();
	var lastUpdate = node.data.lastupdate;
	
	importTree(modelTree, tree);

	//Render the tree
	tree.render(true, true);

	//Re-select the active node
	if (lastUpdate != node.data.lastupdate) {
		doNodeSelection(node.data.id);
	}
}

var autorefreshId = -1;

function dataRefresh() {
	$.getJSON("getmodeltree.php?arcurl=" + arcurl, refreshModelTree);
}

function autorefreshOnOff() {
	var button = $("#autorefresh");
	if ( button.html() == 'Off' ) {
		button.removeClass('btn-danger').addClass('btn-success').addClass('pulse-success').html("On");
		autorefreshId = window.setInterval(dataRefresh, 3000);
	} else {
		window.clearInterval(autorefreshId);
		button.removeClass('btn-success').addClass('btn-danger').removeClass('pulse-success').html("Off");
	}
}

//Page load
$(function() {

	$("#zonename").html(zone);

	//------------------- treetable ---------------------
	var tree = $('#treetable').fancytree({
		extensions: ["table","glyph"],
		selectMode: 1,
		minExpandLevel: 2,
		table: {
			checkboxColumnIdx: 0,   // render the checkboxes into the this column index (default: nodeColumnIdx)
			nodeColumnIdx: 0        // render node expander, icon, and title to this column (default: #0)
		},
		glyph: glyph_opts_model,
		renderColumns: function(event, data) {
			var node = data.node;
			$tdList = $(node.tr).find(">td");
			var conspicuity =  node.data.conspicuity;
			$tdList.eq(1).html('<span class="badge conspicuity-' + conspicuity + '">' + node.data.tags[0] + '</span>');
		},
		activate: function(event, data) {
			var node = data.node;
			doNodeSelection(node.data.id);
		},
		init: function(event, data, flag) {
			var node = data.tree.getRootNode();
			node.getFirstChild().setActive();
		},
		dblclick: function(node, event) {
			//Double click collapses the root node but does not expand it
			//since we don't need double click in this tree we completely disable it
			return false; 
		},
		source : {url: "getmodeltree.php?arcurl=" + arcurl}
	});

	//------------------- exceptiontable ---------------------
	var pageScrollPos = 0;
	var exceptiontable = $('#exceptiontable').DataTable( {
		dom: 'Bfrtp',
		select: true,
		scrollY: 300,
		paging: false,
		order: [[ 3, "desc" ]], // by default all exceptions are ordered by date
		ajax: {
			url: 'getexceptionlist.php?arcurl=' + arcurl + '&nodeid=1&state=RaisedUnaccepted|RaisedAccepted',
			dataSrc: 'exceptions'
		},
		createdRow: function ( row, data, index ) {
			if ( data.exceptionState == "RaisedUnaccepted" ) {
				$('td', row).addClass('highlight');
			}
		},
		preDrawCallback: function (settings) {
			pageScrollPos = $('div.dataTables_scrollBody').scrollTop();
		},
		drawCallback: function (settings) {
			$('div.dataTables_scrollBody').scrollTop(pageScrollPos);

			// enable delete button if any
			var api = this.api();
			var count = api.data().count();
		
			var tree = $('#treetable').fancytree('getTree');
			var node = tree.getActiveNode();

		
			if (( count == 0 ) || (node.title == "Trash") ) 
			{
				api.button( 0 ).enable( false );
			}
			else
			{
				api.button( 0 ).enable( true );
				
			}
		},
		columnDefs: [
			{ targets: 0, orderData: 1, width: "1%", className: "dt-center"}, // it orders the severity bullet according to the severitylevel column
			{ targets: 1, visible: false, type: "num"}, // column to contain severity number
			{ targets: 2, render: $.fn.dataTable.render.ellipsis(40, true, true ), width: "20%"}, // put elipsis for identifier longer than 40 charrs 
			{ targets: 3, width: "1%"},
			{ targets: 5, visible: false} // column containing uuid		
		],
		columns: [
			{ data: 'severity',
				render: function ( data, type, row ) {
					var severity= data.toLowerCase(); // protect against Capital 
					if ( severities.indexOf(severity) != -1 ) {
						return '<span class="severity-' + severity + '">&bull;</span>';
					}
					else
					{
						return '<span class="severity-unknown">&bull;</span>';
					}
				}
			},
			{ data: 'severitylevel'},
			{ data: 'identifier' },
			{ data: 'dateTime'},
			{ data: 'message' ,
				render: function ( data, type, row ) {
					var cutoff = 20;
					data = decodeURIComponent(data); 
					// Order, search and type get the original data
					if ( type !== 'display' ) {
						return data;
					}

					if ( typeof data !== 'number' && typeof data !== 'string' ) {
						return data;
					}

					data = data.toString(); // cast numbers

					if ( data.length <= cutoff ) {
						return data;
					}

					var shortened = data.substr(0, cutoff-1);

					// Find the last white space character in the string
					shortened = shortened.replace(/\s([^\s]*)$/, '');
				

					return '<span class="ellipsis" title="'+data+'">'+shortened+'&#8230;</span>';
				} 
			},
			{ data: 'exception'}
		],
		rowId: 'exception',
		buttons: [
					{
						text: 'Remove',
						enabled: false,
						action: function () {
							var rows = exceptiontable.rows( { selected: true } );
							if ( rows.count() > 0 ) {
							rows.every( function ( rowIdx, tableLoop, rowLoop ) {
								var uuid = this.data()["exception"];
								$.post('clear.php', {id: uuid, arcurl: arcurl});
							});
							}
							else
							{
								$("#invalidselection").modal()
							}
						}
					}
				]
	});

	//------------------- exceptionview ---------------------
	var tree = $('#exceptionview').fancytree({
		extensions: ["glyph"],
		selectMode: 1,
		glyph: glyph_opts_exception,
		activate: function(event, data) {
			var node = data.node;
			var exceptionproperties = $("#exceptionproperties").dataTable().api();

			//Reload of the data
			exceptionproperties.clear();
			exceptionproperties.rows.add(node.data.properties);
			exceptionproperties.draw();
		},
		init: function(event, data, flag) {
			data.tree.visit(function(node){
				node.setExpanded(true);
			});
			var node = data.tree.getRootNode().getFirstChild();
			if (node) {
				node.setActive();
			}
		},
		//source : {url: "getexception.php?id=" + node.getAttribute("id")}
		//source : {url: "getexception.php?id=04f0a976-090a-427f-8a6a-c09c52e31c21"}
		source : []
	});

	exceptiontable.on( 'select', function ( e, dt, type, indexes ) {
		if ( type === 'row' ) {
			var data = dt.rows( indexes ).data();
			var rowData = data.toArray();
			//console.log(JSON.stringify( rowData ));
			var uuid = rowData[0]["exception"];
			if ( rowData[0]["exceptionState"] == "RaisedUnaccepted" ) {
				$.post('accept.php', {id: uuid, arcurl: arcurl});
			}
			selectExceptionRow(uuid);
			
		}
	} );

	exceptiontable.on( 'deselect', function ( e, dt, type, indexes ) {
		//Clearing eceptionproperties
		var exceptionproperties = $("#exceptionproperties").dataTable().api();
		exceptionproperties.clear();
		exceptionproperties.draw();

		//Clearing exceptiontreeview
		var tree = $('#exceptionview').fancytree('getTree');
		tree.reload([]);
	} );

	//------------------- exceptionproperties ---------------------
	var exceptionpropertiestable = $('#exceptionproperties').DataTable( {
		dom: '<"floatRight"B>',
		select: false,
		scrollY: 300,
		paging: false,
		drawCallback : function( settings, json ) {
			var api = this.api();
			var count = api.data().count();

			var tree = $('#exceptionview').fancytree('getTree');
			var root = tree.getRootNode().getFirstChild();
			var node = tree.getActiveNode();
			
			if ( (count == 0) || (root != node ) )
			{
				api.button( 0 ).enable( false );
				api.button( 1 ).enable( false );
			}
			else
			{
				api.button( 0 ).enable( true );
				api.button( 1 ).enable( true );
			}
		 },
		columns: [
			{data: 'name'
			},
			{data: 'value',
				render: function ( data, type, row ) {
					if ( row.name == 'notifier')
						return '<a href="'+ data +'" target="_blank">' + data +'</a>';
					else
						return data;
			}}
		],
		buttons: [
					{
						text: 'JSON',
						enabled: false,
						action: function () {
						
								var tree = $('#exceptionview').fancytree("getTree");
								var properties = tree.activeNode.data.properties;
								for (var i in properties) {
									if (properties[i]['name'] == 'uniqueid' ) 
									{
										var url = 'getexception.php?arcurl=' + arcurl + '&id=' + properties[i]['value'];
										window.open(url,"_blank");
									}
								}
		
						}
					},
					{
						text: 'HTML',
						enabled: false,
						action: function () {
							 var tree = $('#exceptionview').fancytree("getTree");
								var properties = tree.activeNode.data.properties;
								for (var i in properties) {
									if (properties[i]['name'] == 'uniqueid' ) 
									{
										var url = 'getexceptionhtml.php?arcurl=' + arcurl + '&id=' + properties[i]['value'];
										window.open(url,"_blank");
									}
								}
						}
					}
				]
	});

	//Reload the tree every 2 seconds
	//window.setInterval(dataRefresh, 2000);

});

</script>

</head>
<body>


	<div class="container-fluid fullview">

		<nav class="navbar navbar-default navbar-xs">
				<div class="navbar-header" align="left">
					<a class="navbar-brand" href="#">
						<div align="bottom">
							<img alt="Hotspot5" src="images/hotspot-icon.png" height="24px"> <img alt="Hotspot5">
						</div>
					</a>
				</div>
			<div class="collapse navbar-collapse"
				id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav navbar-right">
					<li><a href="#" id="zonename" style="color:green;"></a></li>
				</ul>
				<button data-toggle="tooltip" title="Enable/disable autorefresh" id="autorefresh" onclick="autorefreshOnOff()" type="button" class="btn btn-danger navbar-btn navbar-right btn-xs">Off</button>
			</div>
		</nav>
		<div class="col-sm-3 col-md-3 col-lg-3 sidenav">
			<div class="panel panel-default">
				<table id="treetable"
					class="table table-hover fancytree-fade-expander table-borderless">
					<colgroup>
						<col width="80px"></col>
						<col width="10px"></col>
					</colgroup>
					<tbody>
						<tr>
							<td></td>
							<td></td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
		<div class="col-sm-9 col-md-9 col-lg-9" id="rightnav">
			<div class="row">
				<div id="exceptionlist">
					<table id="exceptiontable" class="display compact nowrap"
						cellspacing="0" width="100%">
						<thead>
							<tr>
								<th>Severity</th>
								<th>Severity Level</th>
								<th>Identifier</th>
								<th>Date</th>
								<th>Message</th>
								<th>UUID</th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-4" id="exceptionview"></div>
				<div class="col-sm-8" id="exceptionpropertiesview">
					<table id="exceptionproperties" class="table">
						<thead>
							<tr>
								<th>Name</th>
								<th>Value</th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
		</div>
	</div>

 <!-- Modal -->
  <div class="modal fade" id="invalidselection" role="dialog">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Invalid Selection</h4>
        </div>
        <div class="modal-body">
          <p>Select one or more exception to delete</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
	<!--  footer class="container-fluid">
		<p>Footer Text</p>
	</footer -->

</body>
</html>
