<?php
require '../utils/vendor/autoload.php';
include_once ('config.php');

function convertHTML($data) {
	echo "<table>";

	// Convert message to proper format
	if (isset( $data ["properties"]) ) {
		foreach ( $data ["properties"] as &$property ) {
			echo "<tr>";
			echo "<td>";
			echo $property ["name"];
			echo "</td>";
			echo "<td>";
			if ($property ["name"] == "message") {
				echo urldecode ( $property ["value"] );
			} else {
				echo $property ["value"];
			}
			echo "</td>";
			echo "</tr>\n";
		}
	}

	echo "</table>";
	
	if (isset( $data ["children"]) && count ( $data ["children"] ) > 0) {
		echo "<br> originated by ";
		foreach ( $data ["children"] as $child ) {
			convertHTML ( $child ) ;
		}
	}
}

$_id = $_GET ['id'];
$_arcurl = $_GET ['arcurl'];

$response = Network::httpgetnocache ( $_arcurl . "/getException?id=" . $_id );
$json = json_decode ( $response->getBody(), true );
header ( 'Content-Type: text/plain; charset=utf-8' );
convertHTML ( $json ) ;

?>
