<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>CMS - Escaped (Elasticsearch capability for enhanced data aquisition)</title>
<link href="css/xdaq-tables.css" rel="stylesheet" />
</head>
<body>

<?php
    
    include_once "config/config.php";
    
    require 'vendor/autoload.php';
    use Httpful\Exception;
    ini_set('display_errors', 'On');
    error_reporting(E_ALL | E_STRICT);
    
    
    // base url for proxy E
    
    foreach($config['hosts'] as $host)
    {
    
    // build url

    $url_ =  "http://" . $host . ":". $config['esport'] ."/_nodes";


    echo $host;
    
    // perform request to ES
    $response = \Httpful\Request::get($url_)->send();
    
    // parse response into object code
    $json = json_decode($response, true);
    
    echo '<br />';
    
    $header_fields = array("host", "ip","name");
       
    // Output table in html
    echo '<table class="xdaq-table">';
    
    echo '<thead>';
    foreach($header_fields as $name)
    {
        
        echo '<th>';
        echo $name;
        echo '</th>';
    }
    echo '</thead>';
    
    echo '<tbody>';
    
    foreach ($json['nodes'] as $key => $val) {
        echo "<tr>";
        //foreach ($val ['_source'] as $key => $field) {
        foreach($header_fields as $name)
        {
            $field = $val[$name];
            echo '<td>';
            echo $field;
          
            
            echo '</td>';
            
        }
        echo "</tr>";
    }
    
    echo '</tbody>';
    echo '</table>';
    }
    
    ?>

</body>
</html>
