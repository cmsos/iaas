<?php

    require 'vendor/autoload.php';
    
    function searchXtremeServices($host, $port)
    {
	print ("going to retrieve ". "http://" . $host . ":" . $port . "/" . "urn:xdaq-application:service=xmasheartbeat/retrieveHeartbeatTable?classname=es::xtreme::Application\n" );
        $response = \Httpful\Request::get( "http://" . $host . ":" . $port . "/" . "urn:xdaq-application:service=xmasheartbeat/retrieveHeartbeatTable?classname=es::xtreme::Application")->send();
        
	//print ("done\n" );
        $json = json_decode($response, true);
        return $json;
    }

    
    function disableXtremeServices($host, $port)
    {
        $json = searchXtremeServices($host, $port);
        
        foreach ($json['table']['rows'] as $row)
        {
	    $context = $row['context'];
	    $lid = $row['id'];
	    $clasname = $row['class'];
	    $age = floatval($row['age']);
    
	   print('going to disable'. $context . ' on lid' . $lid . ' of age ' . $age);
	    if ($age < 1.0) {
		if ( "http://cms-kvm-31.cms:9950" != $context)
		{
       	    	$response = \Httpful\Request::get( $context . "/" . "urn:xdaq-application:lid=". $lid . "/" . "disableESCloud")->send();
		}
	    }
        }
    
    }

    function enableXtremeServices($host, $port)
    {
        $json = searchXtremeServices($host, $port);
        
        foreach ($json['table']['rows'] as $row)
        {
	    $context = $row['context'];
	    $lid = $row['id'];
	    $clasname = $row['class'];
	    $age = floatval($row['age']);
    
	   print('going to enable'. $context . ' on lid' . $lid . ' of age ' . $age);
	    if ($age < 1.0) {
		if ( "http://cms-kvm-31.cms:9950" != $context)
		{
       	    	$response = \Httpful\Request::get( $context . "/" . "urn:xdaq-application:lid=". $lid . "/" . "enableESCloud")->send();
		}
	    }
        }
    
    }

    function configureXtremeServices($host, $port, $path)
    {
        $json = searchXtremeServices($host, $port);

        foreach ($json['table']['rows'] as $row)
        {
            $context = $row['context'];
            $lid = $row['id'];
            $clasname = $row['class'];
            $age = floatval($row['age']);

           print('going to enable'. $context . ' on lid' . $lid . ' of age ' . $age . ' for path '. $path);
		if ( "http://cms-kvm-31.cms:9950" != $context)
		{
            if ($age < 1.0) {
		$commandline = './setAutoconfSearchPath.sh ' .  $lid . ' ' . $context . ' ' . $path;
		$output = shell_exec($commandline);
            }
		}
        }
 
    }

    function clusterXtremeServices($host, $port, $path)
    {
        $json = searchXtremeServices($host, $port);

        foreach ($json['table']['rows'] as $row)
        {
            $context = $row['context'];
            $lid = $row['id'];
            $clasname = $row['class'];
            $age = floatval($row['age']);

           print('going to enable'. $context . ' on lid' . $lid . ' of age ' . $age . ' for path '. $path);
		if ( "http://cms-kvm-31.cms:9950" != $context)
		{
            if ($age < 1.0) {
		$commandline = './setElasticsearchClusterUrl.sh ' .  $lid . ' ' . $context . ' ' . $path;
		$output = shell_exec($commandline);
            }
		}
        }
 
    }


?>
