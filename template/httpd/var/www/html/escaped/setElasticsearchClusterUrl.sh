#!/bin/sh
# usage: 
# setElasticsearchClusterUrlh.sh  61 http://srv-c2d06-17.cms:9950  http://cmsos-iaas-cdaq.cms:9200
#
curl -v -0 -H "SOAPAction: urn:xdaq-application:lid=$1" $2 \
-d "<soap-env:Envelope soap-env:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\" xmlns:soap-env=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"><soap-env:Header/><soap-env:Body><xdaq:ParameterSet xmlns:xdaq=\"urn:xdaq-soap:3.0\"><properties xmlns=\"urn:xdaq-application:es::xtreme::Application\" xsi:type=\"soapenc:Struct\"><elasticsearchClusterUrl xsi:type=\"xsd:string\">$3</elasticsearchClusterUrl></properties></xdaq:ParameterSet></soap-env:Body></soap-env:Envelope>"

