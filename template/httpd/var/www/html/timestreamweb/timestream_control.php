<html>

<head>
<meta charset="utf-8">
<title>CMS - Timestreamweb</title>
<link href="css/timestreamweb.css" rel="stylesheet" />
</head>

<body>
<?php
require 'vendor/autoload.php';
include_once ('tools.php');
if (isset ( $_POST ['control'] )) { // to run PHP script on submit
	if (! empty ( $_POST ['chk_group'] )) {
		// Loop to store and display values of individual checked checkbox.
		echo '<table>' . PHP_EOL;
		echo '<thead>' . PHP_EOL;
		echo '<tr>' . PHP_EOL;
		echo '<th>URL</th>';
		echo '<th>Status</th>';
		echo '</tr>';
		echo '</thead>';
		echo '<tbody>';
		
		foreach ( $_POST ['chk_group'] as $selected ) {
			
			$url = $selected . "/" . $_POST ['control'];
			echo '<tr>';
			echo '<td>';
			echo $url;
			echo '</td>';
			try {
				$response = \Httpful\Request::get ( $url )->send ();
				echo '<td>';
				echo $response->code;
				echo '</td>';
			} catch ( Exception $e ) {
				echo '<td>';
				echo $e->getMessage ();
				echo '</td>';
			}
			echo '</tr>';
		}
		echo '</tbody>';
		echo '</table>';
	}
}

echo '<br>';

echo '<form action="clients.php">';
echo '<input type="submit" value="Go back" method="get">';
echo '<input type="hidden" name="heartbeaturl" value="' . $_POST ['heartbeaturl'] . '"/>';
echo '<input type="hidden" name="zone" value="' . $_POST ['zone'] . '"/>';

echo '</form>'?>

</body>
</html>