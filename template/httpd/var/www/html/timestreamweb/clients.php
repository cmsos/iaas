<!doctype html>
<html>

<head>
<meta charset="utf-8">
<title>CMS - Timestreamweb</title>
<link href="css/timestreamweb.css" rel="stylesheet" />
<script type="text/javascript" src="js/toggle.js"></script>
</head>

<body>

<?php
include_once ('config.php');
include_once ('tools.php');
include_once ('heartbeat.php');
include_once ('timestream.php');

ini_set ( 'display_errors', 'On' );
error_reporting ( E_ALL | E_STRICT );

$_heartbeaturl = $_GET ['heartbeaturl'];
$_zone = $_GET ['zone'];

$headerTitle = 'Timestream XDAQ applications for "' . $_zone . '" zone';
include_once ('header.php');

echo '<form id="timestreamForm" action="" method="post">';
echo '<table>';
echo '<thead>';
echo '<tr>';
echo '<th><input type="checkbox" onClick="toggle(this)" value="all"/></th>';
echo '<th>Timestream</th>';
echo '<th>Status</th>';
echo '</tr>';
echo '</thead>';

echo '<tbody>';

$timestreams = searchTimestreamServices ( $_heartbeaturl );

foreach ( $timestreams ['table'] ['rows'] as $row ) {
	$context = $row ['context'];
	$lid = $row ['id'];
	$clasname = $row ['class'];
	$age = floatval ( $row ['age'] );
	
	$timestreamurl = $context . "/" . "urn:xdaq-application:lid=" . $lid;
	
	if ($age < 1.0) {
		echo '<tr>';
		echo '<td>';
		echo '<input type="checkbox" name="chk_group[]" value="' . $timestreamurl . '"/>';
		echo '</td>';
		echo '<td>';
		echo '<a href="' . $timestreamurl . '">' . $timestreamurl . '</a>';
		echo '</td>';
		echo '<td>';
		try {
			$status = getTimestreamStatus ( $context, $lid );
			echo ($status ? '<font color="green">enabled<font>' : '<font color="red">disabled<font>');
		} catch ( Exception $e ) {
			echo 'Caught exception: ', $e->getMessage (), "\n";
		}
		echo '</td>';
		echo '</tr>';
	}
}

echo '</tbody>';
echo '</table>';
echo '<br>';

echo '<input type="hidden" name="heartbeaturl" value="' . $_heartbeaturl . '"/>';
echo '<input type="hidden" name="zone" value="' . $_zone . '"/>';

echo '<input type="submit" name="control" value="disable" onclick="doControl()"/>';
echo '<input type="submit" name="control" value="enable" onclick="doControl()"/>';
echo '</form>';

?>

<script>
form = document.getElementById("timestreamForm");

function doControl() {
	form.action="timestream_control.php";
	form.submit();
}
</script>

	<br>

	<form action="index.php">
		<input type="submit" value="Go back" method="get" />
	</form>

</body>
<footer>Copyright © XDAQ 2016</footer>

</html>

