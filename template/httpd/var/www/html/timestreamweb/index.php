<!doctype html>
<html>

<head>
<meta charset="utf-8">
<title>CMS - Timestreamweb</title>
<link href="css/timestreamweb.css" rel="stylesheet" />
<script type="text/javascript" src="js/toggle.js"></script>
</head>

<body>

<?php
$headerTitle='CMS Timestreamweb';
include_once ('header.php');
include_once ('config.php');
include_once ('tools.php');
include_once ('smarthub.php');

ini_set ( 'display_errors', 'On' );
error_reporting ( E_ALL | E_STRICT );

echo '<table>';
echo '<thead>';
echo '<tr>';
echo '<th>Zone</th>';
echo '<th>Heartbeat URL</th>';
echo '</tr>';
echo '</thead>';

echo '<tbody>';

$zones = $zones_;

if ( $smarthuburl_ != '' )
{
	$dynamiczones = findAllZones($smarthuburl_);

	if ( is_array($dynamiczones) )
	{
		$zones = array_merge($zones_, $dynamiczones ); // find all zones dynamically
	}
}

foreach ( $zones as $key => $val ) {
	$zonename = $key;
	echo '<tr>';
	echo '<td>';
	echo '<a href="' . 'clients.php?zone=' . $zonename . '&heartbeaturl=' . urlencode($val) . '">' . $zonename . '</a>';
	echo '</td>';
	echo '<td>';
	echo '<a href="' . $val . '">' . $val . '</a>';
	echo '</td>';
	echo '</tr>';
}
echo '</tbody>';
echo '</table>';

?>

<div>

<footer>Copyright © XDAQ 2016</footer>

</body>
</html>
