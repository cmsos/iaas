<?php
require 'vendor/autoload.php';
function searchSmarthubServices($url) {
	$response = \Httpful\Request::get ( $url . "/" . "urn:xdaq-application:service=xplore/search?filter=(service%3Dxmasheartbeat)&service=peer" )->send ();
	$json = json_decode ( $response, true );
	return $json;
}

function findAllZones($url)
{
	try
	{
		$zones = searchSmarthubServices($url);
	}
	catch (Exception $e)
	{
		return NULL;
	}
	
	foreach ( $zones ['table'] ['rows'] as $row ) {

		$context = $row ['context'];
		$lid = $row ['id'];
		$service = $row ['service'];
		$zone = $row ['zone'];

		$zonesurls[$zone] = $context;
	}
	return $zonesurls;
}
?>
