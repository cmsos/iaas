<!doctype html>
<html>

<head>
<meta charset="utf-8">
<title>CMS - Flashweb</title>
<link href="css/flashweb.css" rel="stylesheet" />
<script type="text/javascript" src="js/toggle.js"></script>
</head>

<body>

<?php
include_once ('config.php');
include_once ('tools.php');

ini_set ( 'display_errors', 'On' );
error_reporting ( E_ALL | E_STRICT );

$_zone = $_GET ['zone'];
$_tag = $_GET ['tag'];
$_flashlist = $_GET ['flashlist'];
$_prefix = "cmsos-data-" . $_zone . "-" . $_tag . "-" . strtolower ( $_flashlist ) . "_";

$headerTitle = 'Indices of "' . $_flashlist . '" flashlist "' . $_zone . '" zone "' . $_tag . '" tag (version)';
include_once ('header.php');

// Adding combo box with all the tags of the zone
$tags = retrieveTags($config ['elasticsearchurl'], $_zone);

if (count($tags) > 0) {
	if ( ($_tag == '') or ( !in_array($_tag, $tags) ) ) {
		$_tag = $tags[0];
	}
}

echo '<form id="tagCombo" action="" method="post">' . PHP_EOL;
echo 'Current tag (version): ' . PHP_EOL;
echo '<select autocomplete="off" id="tagCombo" onchange="doTagChange(\'' . $_zone . '\', this.options[this.selectedIndex].value, \'' . $_flashlist . '\')">' . PHP_EOL;
foreach ($tags as $tag) {	
	echo '<option value="' . $tag . '"';
	if ($tag == $_tag) {
		echo ' selected';
	}
	echo '>' . $tag . '</option>' . PHP_EOL;
}
echo '</select>' . PHP_EOL;
echo '</form>' . PHP_EOL;

echo '<form action="delete_indices.php" method="post">';
echo '<table>';
echo '<thead>';
echo '<tr>';
echo '<th><input type="checkbox" onClick="toggle(this)" value="all"/></th>';
echo '<th>Index</th>';
echo '<th>Document count</th>';
echo '<th>Index properties</th>';
echo '<th>Last document</th>';
echo '<th>Aliases</th>';
echo '<th>Last update</th>';
echo '<th>Expires in</th>';
echo '<th>Closing in</th>';
echo '<th>State</th>';
echo '<th>Store size</th>';
echo '</tr>';
echo '</thead>';

echo '<tbody>';
function sortFunction($a, $b) {
	return ( int ) ($b ["settings"] ["index"] ["creation_date"]) - ( int ) ($a ["settings"] ["index"] ["creation_date"]);
}

$indices = retrieveIndices ( $config ['elasticsearchurl'], $_prefix . "*_*" );

uasort ( $indices, "sortFunction" );

foreach ( $indices as $key => $val ) {
	$index_name = $key;
	
	$mappings = $val ["mappings"];
	$aliases = $val ["aliases"];
	if (validateSignature ( $_flashlist, $mappings, $config ['timestreamsignature'] )) {
		$starttime = $mappings [$_flashlist] ["_meta"] ["starttime"];
		$timeinterval = $mappings [$_flashlist] ["_meta"] ["timeinterval"];
		$timetolive = $mappings [$_flashlist] ["_meta"] ["timetolive"];
		
		echo '<tr>';
		echo '<td>';
		echo '<input type="checkbox" name="chk_group[]" value="' . $index_name . '"/>';
		echo '</td>';
		echo '<td>';
		echo $index_name;
		echo '</td>';
		echo '<td>';
		echo documentCount ( $config ['elasticsearchurl'], $index_name );
		echo '</td>';
		echo '<td>';
		echo '<a href="view_index.php?zone=' . $_zone . '&tag=' . $_tag . '&flashlist=' . $_flashlist . '&indexname=' . $index_name . '">View</a>';
		echo '</td>';
		echo '<td>';
		echo '<a href="view_last_document.php?zone=' . $_zone . '&tag=' . $_tag . '&flashlist=' . $_flashlist . '&indexname=' . $index_name . '">View</a>';
		echo '</td>';
		echo '<td>';
		echo printAliases ( $aliases );
		echo '</td>';
		echo '<td>';
		echo lastUpdate ( $config ['elasticsearchurl'], $index_name );
		echo '</td>';
		echo '<td>';
		$expiry = hasExpired ( $starttime, $timeinterval, $timetolive );
		echo ($expiry->invert ? '<font color="red">-' : '<font>') . $expiry->format ( '%a days %H:%I:%S' ) . '</font>';
		echo '</td>';
		echo '<td>';
		if (isset ( $mappings [$_flashlist] ["_meta"] ["openinterval"] )) {
			$openinterval = $mappings [$_flashlist] ["_meta"] ["openinterval"];
			$closing = hasExpired ( $starttime, $timeinterval, $openinterval );
			echo ($closing->invert ? '<font color="orange">-' : '<font>') . $closing->format ( '%a days %H:%I:%S' ) . '</font>';
		} else {
			echo 'n.a.';
		}
		echo '</td>';
		echo '<td>';
		$state = getIndexState ( $config ['elasticsearchurl'], $index_name );
		echo ($state == "open" ? '<font color="green">' : '<font color="red">') . $state . '</font>';
		echo '</td>';
		echo '<td>';
		echo getStoreSize ( $config ['elasticsearchurl'], $index_name );
		echo '</td>';
		echo '</tr>';
	}
}
echo '</tbody>';
echo '</table>';

echo '<br>';

echo '<input type="hidden" name="zone" value="' . $_zone . '"/>';
echo '<input type="hidden" name="tag" value="' . $_tag . '"/>';
echo '<input type="hidden" name="flashlist" value="' . $_flashlist . '"/>';
echo '<input type="hidden" name="elasticsearchurl" value="' . $config ['elasticsearchurl'] . '"/>';
echo '<input type="submit" name="delete" value="Delete selection"/>';
echo '</form>';

echo '<br>';

echo '<form action="types.php">';
echo '<input type="submit" value="Go back" method="get">';
echo '<input type="hidden" name="zone" value="' . $_zone . '"/>';
echo '<input type="hidden" name="tag" value="' . $_tag . '"/>';
echo '</form>'?>

<script>
form = document.getElementById("tagCombo");

function doTagChange(zone, tag, flashlist) {
	form.action="indices.php?zone=" + zone + "&tag=" + tag + "&flashlist=" + flashlist;
	form.submit();
}
</script>

</body>
<footer>Copyright © XDAQ 2019</footer>

</html>

