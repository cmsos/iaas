<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>CMS - Flashweb</title>
<link href="css/flashweb.css" rel="stylesheet" />

<script type="text/javascript" src="jquery/jquery-3.1.0.min.js"></script>
<script type="text/javascript">
$(function () {

 $('#container').highcharts({
        chart: {
            type: 'scatter',
            zoomType: 'xy'
        },
        title: {
            text: 'Flashlist refresh latency'
        },
        subtitle: {
            text: 'Source: XDAQ 2019'
        },
        xAxis: {
            title: {
                enabled: true,
                text: 'Latency (s)'
            },
            startOnTick: true,
            endOnTick: true,
            showLastLabel: true,
            type: 'logarithmic',
            allowDecimals: false,
            gridLineWidth: 1
        },
        yAxis: {
            title: {
                text: 'Source #'
            },
            allowDecimals: false
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'top',
            floating: true,
            backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF',
            borderWidth: 1
        },
        plotOptions: {
            scatter: {
                marker: {
                    radius: 5,
                    states: {
                        hover: {
                            enabled: true,
                            lineColor: 'rgb(100,100,100)'
                        }
                    }
                },
                states: {
                    hover: {
                        marker: {
                            enabled: false
                        }
                    }
                },
                tooltip: {
                    headerFormat: '<b>{series.name}</b><br>',
                    pointFormat: '{point.x} secs {point.y} sources',
                    shared: true
                }
            }
        },

<?php
include_once ('config.php');
include_once ('tools.php');
include_once ('las.php');

echo 'series: [';

$inputFieldsHTML = '';
$_zone = $_POST ['zone'];
$_tag = $_POST ['tag'];
if (! empty ( $_POST ['chk_group'] )) {
	// Loop to store and display values of individual checked checkbox.
	$inputFieldsHTML = $inputFieldsHTML . '<input type="hidden" name="zone" value="' . $_zone . '"/>';
	$inputFieldsHTML = $inputFieldsHTML . '<input type="hidden" name="tag" value="' . $_tag . '"/>';
	$inputFieldsHTML = $inputFieldsHTML . '<input type="hidden" name="elasticsearchurl" value="' . $config ['elasticsearchurl'] . '"/>';
	$lastFlashlist = end ( $_POST ['chk_group'] );
	foreach ( $_POST ['chk_group'] as $flashlist ) {
		$flashing_alias = "cmsos-data-" . $_zone . "-" . $_tag . "-" . strtolower ( $flashlist ) . "-flash";

		// Current time in milliseconds
		$currenttime = round ( microtime ( true ) * 1000 );
		echo '{' . PHP_EOL;
		echo "name: '" . $flashlist . "'," . PHP_EOL;
		echo 'data: [';

		$filter = array ();
		$tophits = topHits ( $config ['elasticsearchurl'], $flashing_alias, $flashlist, $config ['maxsources'], $filter );
		
		$latencies = array ();
		foreach ( $tophits ['aggregations'] ['group_by_context'] ['buckets'] as $bucket ) {
			foreach ( $bucket ['top_metrics'] ['hits'] ['hits'] as $hits ) {
				$creationtime = 0;
				if (isset ( $hits ['_source'] ['creationtime_'] )) {
					$creationtime = $hits ['_source'] ['creationtime_'];
					// Latency in seconds
					$latency = max ( round ( ($currenttime - $creationtime) / 1000 ), 1);
					$latencies[$latency]++;
				}
			}
		}
		
		$last_key = end ( array_keys( $latencies ) );
		foreach ( $latencies as $key => $value ) {
			// echo "{$key} => {$value} ";
			echo '[' . $key . ',' . $value . ']';
			if ( $key != $last_key ) {
				echo ', ';
			}
		}
		echo ']' . PHP_EOL;
		echo '}';
		if ( $flashlist != $lastFlashlist ) {
			echo ', ' . PHP_EOL;
		}
		
		// Generate input hidden fields
		$inputFieldsHTML = $inputFieldsHTML . '<input type="hidden" name="chk_group[]" value="' . $flashlist . '">';
	}
	echo ']' . PHP_EOL;
}

echo '})';
echo '})';

echo '</script>';
echo '</head>';
echo '<body>';

echo '<script src="highcharts/js/highcharts.js"></script>';
echo '<script src="highcharts/js/highcharts-more.js"></script>';
echo '<script src="highcharts/js/modules/data.js"></script>';
echo '<script src="highcharts/js/modules/exporting.js"></script>';

echo '<div id="container" style="min-width: 420px; max-width: 1600px; height: 800px; margin: 0 auto"></div>';

echo '<form id="flashlistsForm" action="latency_scatter_plot.php" method="post">';
echo $inputFieldsHTML;
echo '<input type="submit" value="Refresh" method="post"/>';
echo '</form>';

echo '<form action="types.php">';
echo '<input type="hidden" name="zone" value="' . $_zone . '"/>';
echo '<input type="hidden" name="tag" value="' . $_tag . '"/>';
echo '<input type="submit" value="Go back" method="get">';
echo '</form>';

echo '</body>';
echo '</html>';
?>
