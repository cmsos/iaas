<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>CMS - Flashweb</title>
<link href="css/flashweb.css" rel="stylesheet" />

<script type="text/javascript" src="jquery/jquery-3.1.0.min.js"></script>
<script type="text/javascript">
$(function () {

    // Parse the data from an inline table using the Highcharts Data plugin
    $('#container').highcharts({
        data: {
            table: 'latency',
            startRow: 1,
            endRow: 17,
            endColumn: 7
        },

        chart: {
            polar: true,
            type: 'column'
        },

        title: {
            text: 'Monitoring Latency'
        },

        subtitle: {
            text: '<?php echo $_POST ['zone'];?> zone'
        },

        pane: {
            size: '85%'
        },

        legend: {
            align: 'right',
            verticalAlign: 'top',
            y: 100,
            layout: 'vertical'
        },

        xAxis: {
            tickmarkPlacement: 'on'
        },

        yAxis: {
            min: 0,
            endOnTick: false,
            showLastLabel: true,
            title: {
                text: 'Data sources'
            },
            labels: {
                formatter: function () {
                    return this.value;
                }
            },
            reversedStacks: false
        },

        tooltip: {
            valueSuffix: ''
        },

        plotOptions: {
            series: {
                stacking: 'normal',
                shadow: false,
                groupPadding: 0,
                pointPlacement: 'on'
            }
        }
    });
});
		</script>
</head>
<body>
	<script src="highcharts/js/highcharts.js"></script>
	<script src="highcharts/js/highcharts-more.js"></script>
	<script src="highcharts/js/modules/data.js"></script>
	<script src="highcharts/js/modules/exporting.js"></script>

	<div id="container"
		style="min-width: 420px; max-width: 800px; height: 800px; margin: 0 auto"></div>

	<div style="display: none">
		<table id="latency" border="0" cellspacing="0" cellpadding="0">
			<tr nowrap bgcolor="#CCCCFF">
				<th colspan="8" class="hdr">Table of Latencies (seconds)</th>
			</tr>

<?php
include_once ('config.php');
include_once ('tools.php');
include_once ('las.php');

//Validates if float is in range
function validateFloatRange($var, $options) {
	if (isset ( $options ["options"] ["min_range"] )) {
		if ($var < $options ["options"] ["min_range"]) {
			return false;
		}
	}
	if (isset ( $options ["options"] ["max_range"] )) {
		if ($var > $options ["options"] ["max_range"]) {
			return false;
		}
	}
	return true;
}
$latency_ranges = array (
		'<1s' => array (
				'options' => array (
						'max_range' => 1 
				) 
		),
		'1-4s' => array (
				'options' => array (
						'min_range' => 1,
						'max_range' => 4 
				) 
		),
		'4-10s' => array (
				'options' => array (
						'min_range' => 4,
						'max_range' => 10 
				) 
		),
		'10-60s' => array (
				'options' => array (
						'min_range' => 10,
						'max_range' => 60 
				) 
		),
		'1-10min' => array (
				'options' => array (
						'min_range' => 60,
						'max_range' => 600 
				) 
		),
		'10min-1h' => array (
				'options' => array (
						'min_range' => 600,
						'max_range' => 3600 
				) 
		),
		'>1h' => array (
				'options' => array (
						'min_range' => 3600 
				) 
		) 
);

echo '<tr nowrap bgcolor="#CCCCFF">' . PHP_EOL;
echo '<th class="latency">Direction</th>' . PHP_EOL;
foreach ( $latency_ranges as $key => $value ) {
	echo '<th class="latency">' . htmlspecialchars ( $key ) . '</th>' . PHP_EOL;
}
echo '</tr>' . PHP_EOL;

$_zone = $_POST ['zone'];
$_tag = $_POST ['tag'];
if (! empty ( $_POST ['chk_group'] )) {
	// Loop to store and display values of individual checked checkbox.
	$inputFieldsHTML = '';
	$inputFieldsHTML = $inputFieldsHTML . '<input type="hidden" name="zone" value="' . $_zone . '"/>';
	$inputFieldsHTML = $inputFieldsHTML . '<input type="hidden" name="tag" value="' . $_tag . '"/>';
	$inputFieldsHTML = $inputFieldsHTML . '<input type="hidden" name="elasticsearchurl" value="' . $config ['elasticsearchurl'] . '"/>';
	foreach ( $_POST ['chk_group'] as $flashlist ) {
		$flashing_alias = "cmsos-data-" . $_zone . "-" . $_tag . "-" . strtolower ( $flashlist ) . "-flash";
		
		// Current time in milliseconds
		$currenttime = round ( microtime ( true ) * 1000 );
		
		$filter = array ();
		$tophits = topHits ( $config ['elasticsearchurl'], $flashing_alias, $flashlist, $config ['maxsources'], $filter );
		
		$latencies = array ();
		foreach ( $latency_ranges as $key => $value ) {
			$latencies [$key] = 0;
		}
		foreach ( $tophits ['aggregations'] ['group_by_context'] ['buckets'] as $bucket ) {
			foreach ( $bucket ['top_metrics'] ['hits'] ['hits'] as $hits ) {
				$creationtime = 0;
				if (isset ( $hits ['_source'] ['creationtime_'] )) {
					$creationtime = $hits ['_source'] ['creationtime_'];
					// Latency in seconds
					$latency = ($currenttime - $creationtime) / 1000;
					foreach ( $latency_ranges as $key => $value ) {
						if (validateFloatRange ( $latency, $value )) {
							$latencies [$key] ++;
							break;
						}
					}
				}
			}
		}
		
		echo '<tr nowrap>' . PHP_EOL;
		echo '<td class="flashlist">' . $flashlist . '</td>' . PHP_EOL;
		foreach ( $latency_ranges as $key => $value ) {
			echo '<td class="data">' . $latencies [$key] . '</td>' . PHP_EOL;
		}
		echo '</tr>' . PHP_EOL;
		
		// Generate input hidden fields
		$inputFieldsHTML = $inputFieldsHTML . '<input type="hidden" name="chk_group[]" value="' . $flashlist . '">';
	}
}

echo '</table>';
echo '</div>';

echo '<form id="flashlistsForm" action="latency_wind_rose.php" method="post">';
echo $inputFieldsHTML;
echo '<input type="submit" value="Refresh" method="post"/>';
echo '</form>';

echo '<form action="types.php">';
echo '<input type="hidden" name="zone" value="' . $_zone . '"/>';
echo '<input type="hidden" name="tag" value="' . $_tag . '"/>';
echo '<input type="submit" value="Go back" method="get">';
echo '</form>';
?>



</body>
</html>
