<!doctype html>
<html>

<head>
<meta charset="utf-8">
<title>CMS - Flashweb</title>
<link href="css/flashweb.css" rel="stylesheet" />
<script type="text/javascript" src="js/toggle.js"></script>
</head>

<body>

<?php
include_once ('config.php');
include_once ('tools.php');

ini_set ( 'display_errors', 'On' );
error_reporting ( E_ALL | E_STRICT );

$_zone = $_GET ['zone'];

if(isset($_GET ['tag'])) {
	$_tag = $_GET ['tag'];
} else {
	$_tag = '';
}

$headerTitle = 'Flashlists for "' . $_zone . '" zone';
include_once ('header.php');

// Adding combo box with all the tags of the zone
$tags = retrieveTags($config ['elasticsearchurl'], $_zone);

if (count($tags) > 0) {
	if ( ($_tag == '') or ( !in_array($_tag, $tags) ) ) {
		$_tag = $tags[0];
	}
}

$releases = retrieveAllReleases($config ['elasticsearchurl'], $_zone);

//Autotags
echo '<table id="tagsframe">' . PHP_EOL;
echo '<thead>' . PHP_EOL;
echo '<tr>' . PHP_EOL;
echo '<th>Release</th>' . PHP_EOL;
echo '<th>Production</th>' . PHP_EOL;
echo '<th>Staging</th>' . PHP_EOL;
echo '</tr>' . PHP_EOL;
echo '</thead>' . PHP_EOL;
foreach ($releases as $major) {
	echo '<tr>' . PHP_EOL;
	echo '<td>' . $major . '</td>' . PHP_EOL;
	echo '<td>' . retrieveLatestTag($config ['elasticsearchurl'], $_zone, "production_" . $major) . '</td>' . PHP_EOL;
	echo '<td>' . retrieveLatestTag($config ['elasticsearchurl'], $_zone, "staging_" . $major) . '</td>' . PHP_EOL;
	echo '</tr>' . PHP_EOL;
}
echo '</table>' . PHP_EOL;

//Current tag
echo '<table id="tagsframe">' . PHP_EOL;
echo '<thead>' . PHP_EOL;
echo '<tr>' . PHP_EOL;
echo '<th>Current tag</th>' . PHP_EOL;
echo '<th>Flashlists</th>' . PHP_EOL;
echo '<th>Collector</th>' . PHP_EOL;
echo '<th>Sensors</th>' . PHP_EOL;
echo '<th>Pulsers</th>' . PHP_EOL;
echo '<th>Slashes</th>' . PHP_EOL;
echo '</tr>' . PHP_EOL;
echo '</thead>' . PHP_EOL;
echo '<tr>' . PHP_EOL;
echo '<td>' . PHP_EOL;

echo '<form id="tagCombo" action="" method="post">' . PHP_EOL;
echo '<select autocomplete="off" id="tagCombo" onchange="doTagChange(\'' . $_zone . '\', this.options[this.selectedIndex].value)">' . PHP_EOL;
foreach ($tags as $tag) {	
	echo '<option value="' . $tag . '"';
	if ($tag == $_tag) {
		echo ' selected';
	}
	echo '>' . $tag . '</option>' . PHP_EOL;
}
echo '</select>' . PHP_EOL;
echo '</form>' . PHP_EOL;

echo '</td>' . PHP_EOL;
echo '<td><a href="view_metaregistries.php?zone=' . $_zone . '&tag=' . $_tag . '&doctype=flashlists">View</a></td>';
echo '<td><a href="view_metaregistries.php?zone=' . $_zone . '&tag=' . $_tag . '&doctype=collector">View</a></td>';
echo '<td><a href="view_metaregistries.php?zone=' . $_zone . '&tag=' . $_tag . '&doctype=sensors">View</a></td>';
echo '<td><a href="view_metaregistries.php?zone=' . $_zone . '&tag=' . $_tag . '&doctype=pulsers">View</a></td>';
echo '<td><a href="view_metaregistries.php?zone=' . $_zone . '&tag=' . $_tag . '&doctype=slashes">View</a></td>';
echo '</table>' . PHP_EOL;

echo '<form id="flashlistsForm" action="" method="post">';
echo '<table>';
echo '<thead>';
echo '<tr>';
echo '<th><input type="checkbox" onClick="toggle(this)" value="all"/></th>';
echo '<th>Flashlist</th>';
echo '<th>Index count</th>';
echo '<th>Top hits</th>';
echo '<th>Top hits count</th>';
echo '<th>Template properties</th>';
echo '<th>Last update</th>';
echo '<th>Rotate status</th>';
echo '</tr>';
echo '</thead>';

echo '<tbody>';

$templates = retrieveTemplates ( $config ['elasticsearchurl'], 'cmsos-data-' . $_zone . '-' . $_tag . '-*' );

foreach ( $templates as $key => $val ) {
	// Retrieving flashlist name from mappings. We rely on the fact that we will have only one type in every template/index
	$mappings = $val ["mappings"];
	// returns a key of the first element of the array $mappings
	$flashlist = key ( $mappings );
	if (validateSignature ( $flashlist, $mappings, $config ['timestreamsignature'] )) {
		$fullAliasName = 'cmsos-data-' . $_zone . '-' . $_tag . '-' . strtolower ( $flashlist );
		$flashAliasName = $fullAliasName . "-flash";
		echo '<tr>';
		echo '<td>';
		echo '<input type="checkbox" name="chk_group[]" value="' . $flashlist . '"/>';
		echo '</td>';
		echo '<td>';
		echo '<a href="indices.php?zone=' . $_zone . '&tag='. $_tag . '&flashlist=' . $flashlist . '">' . $flashlist . '</a>';
		echo '</td>';
		echo '<td>';
		// Using full alias for index count
		echo indexCount ( $config ['elasticsearchurl'], $fullAliasName );
		echo '</td>';
		echo '<td>';
		echo '<a href="retrieveCollection.php?fmt=html&zone=' . $_zone . '&tag=' . $_tag . '&flash=urn:xdaq-flashlist:' . $flashlist . '">View</a>';
		echo '</td>';
		echo '<td>';
		echo '<a href="view_top_hits_count.php?zone=' . $_zone . '&tag=' . $_tag . '&flashlist=' . $flashlist . '">View</a>';
		echo '</td>';
		echo '<td>';
		echo '<a href="view_template.php?zone=' . $_zone . '&tag=' . $_tag . '&flashlist=' . $flashlist . '">View</a>';
		echo '</td>';
		echo '<td>';
		// Using full alias for last update
		echo lastUpdate ( $config ['elasticsearchurl'], $flashAliasName );
		echo '</td>';
		echo '<td>';
		$status = getRotateStatus ( $config ['elasticsearchurl'], $_zone, $_tag, $flashlist );
		echo ($status ? '<font color="green">enabled<font>' : '<font color="red">disabled<font>');
		echo '</td>';
		echo '</tr>';
	}
}
echo '</tbody>';
echo '</table>';
echo '<br>';

echo '<input type="hidden" name="zone" value="' . $_zone . '"/>';
echo '<input type="hidden" name="tag" value="' . $_tag . '"/>';
echo '<input type="hidden" name="elasticsearchurl" value="' . $config ['elasticsearchurl'] . '"/>';
echo '<input type="submit" name="delete" value="Delete selection" onclick="doDelete()"/>';
echo '<input type="submit" value="Latency scatter plot" onclick="doDisplayLatencyScatter()"/>';
echo '<input type="submit" value="Latency wind rose" onclick="doDisplayLatencyWindRose()"/>';
echo '<input type="submit" name="enable" value="Enable rotate" onclick="doSetRotate()"/>';
echo '<input type="submit" name="disable" value="Disable rotate" onclick="doSetRotate()"/>';
echo '</form>';

?>

<script>
form = document.getElementById("flashlistsForm");

function doDelete() {
	form.action="delete_templates.php";
	form.submit();
}

function doDisplayLatencyScatter() {
	form.action="latency_scatter_plot.php";
	form.submit();
}

function doDisplayLatencyWindRose() {
	form.action="latency_wind_rose.php";
	form.submit();
}

function doSetRotate() {
	form.action="set_rotate.php";
	form.submit();
}

formCombobox = document.getElementById("tagCombo");

function doTagChange(zone, tag) {
	formCombobox.action="types.php?zone=" + zone + "&tag=" + tag;
	formCombobox.submit();
}
</script>

<br>

	<form action="index.php">
		<input type="submit" value="Go back" method="get"/>
	</form>

</body>
<footer>Copyright © XDAQ 2019</footer>

</html>

