<?php
require 'vendor/autoload.php';
include_once ('tools.php');
include_once ('las.php');
include_once ('config.php');

ini_set ( 'display_errors', 'On' );
error_reporting ( E_ALL | E_STRICT );
// get args
$fmt_ = $_GET ["fmt"];
$qflash_ = $_GET ["flash"];
$tophits_ = true;
$zone_ = $_GET ["zone"];
$tag_ = $_GET ["tag"];
$maxsources_ = $config ['maxsources'];
$delimiter_ = ',';

$parts = explode ( ':', $qflash_, 3 );

// $parts[0];
// $parts[1];
$flash_ = $parts [2];
$index_ = "cmsos-data-" . $zone_ . "-" . $tag_ . "-" . strtolower($flash_) . "-flash";

if (! empty ( $_GET ["tophits"] )) {
	$val = $_GET ["tophits"];
	if ($val == "false") {
		$tophits_ = false;
	}
}

if (! empty ( $_GET ["maxsources"] )) {
	$maxsources_ = $_GET ["maxsources"];
}

if (! empty ( $_GET ["index"] )) {
	$index_ = $_GET ["index"];
}

if (! empty ( $_GET ["delimiter"] )) {
	$delimiter_ = $_GET ["delimiter"];
}

$filter = array ();
// Additional query paramaters will be used as filter matching flashlist fields (e.g. sessionid )
$parameters = explode ( "&", $_SERVER ["QUERY_STRING"] );
foreach ( $parameters as $pair ) {
	$keyVal = explode ( '=', $pair );
	$key = &$keyVal [0];
	if ($key != "zone" and $key != "tag" and $key != "fmt" and $key != "flash" and $key != "tophits" and $key != "maxsources" and $key != "index" and $key != "delimiter" and $key != "request") {
		$val = $keyVal [1];
		$filter [$key] = $val;
	}
	// echo "[".$key."]->[".urldecode($val)."]";
	// echo "";
}

if ($fmt_ == "csv") {
	header ( 'Content-type: application/csv' );
	header ( 'Content-Disposition: attachment; filename="' . $flash_ . '.csv"' );
	header ( "Cache-Control: no-cache, must-revalidate" ); // HTTP/1.1
	
	try {
		if ($tophits_)
			retrieveTopHitsCSV ( $config ['elasticsearchurl'], $zone_, $tag_, $index_, $flash_, $maxsources_, $delimiter_, $filter );
		else
			retrieveCSV ( $config ['elasticsearchurl'], $zone_, $tag_, $index_, $flash_, $maxsources_, $delimiter_, $filter );
	}
	catch(Exception $e) {
		header ( "HTTP/1.1 404 Not Found" );
	}
} else if ($fmt_ == "html") {
	try {
		retrieveTopHitsHTML ( $config ['elasticsearchurl'], $index_, $flash_, $maxsources_, $filter );
	}
	catch(Exception $e) {
		header ( "HTTP/1.1 404 Not Found" );
	}
} else if ($fmt_ == "json") {
	
	header ( 'Content-type: application/json' );
	header ( 'Content-Disposition: attachment; filename="' . $flash_ . '.json"' );
	header ( "Cache-Control: no-cache, must-revalidate" ); // HTTP/1.1
	
	try {
		if ($tophits_)
			retrieveTopHitsJSON ( $config ['elasticsearchurl'], $index_, $flash_, $maxsources_, false, $filter );
		else
			retrieveJSON ( $config ['elasticsearchurl'], $index_, $flash_, $maxsources_, false, $filter );
	}
	catch(Exception $e) {
		header ( "HTTP/1.1 404 Not Found" );
	}
} else if ($fmt_ == "plain") {
	header ( 'Content-type: text/plain' );
	
	// print_r($filter);
	
	try {
		if ($tophits_)
			retrieveTopHitsCSV ( $config ['elasticsearchurl'], $zone_, $tag_, $index_, $flash_, $maxsources_, $delimiter_, $filter );
		else
			retrieveCSV ( $config ['elasticsearchurl'], $zone_, $tag_, $index_, $flash_, $maxsources_, $delimiter_, $filter );
	}
	catch(Exception $e) {
		header ( "HTTP/1.1 404 Not Found" );
	}
} else {
	header ( "HTTP/1.1 404 Not Found" );
}
?>
