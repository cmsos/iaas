#!/usr/bin/php
<?php
include_once ('config.php');
include_once ('tools.php');

$_force = false;
if (isset ( $_GET ['force'] )) {
	$_force = $_GET ['force'];
	settype ( $_force, 'boolean' );
}

indexRotate($config ['elasticsearchurl'], $config ['timestreamsignature'], '*', '*', $_force);

?>
