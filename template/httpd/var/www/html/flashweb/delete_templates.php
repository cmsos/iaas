<html>

<head>
<meta charset="utf-8">
<title>CMS - Flashweb</title>
<link href="css/flashweb.css" rel="stylesheet" />
</head>

<body>
<?php
include_once ('tools.php');
$_zone = $_POST ['zone'];
$_tag = $_POST ['tag'];
if (isset ( $_POST ['delete'] )) { // to run PHP script on submit
	if (! empty ( $_POST ['chk_group'] )) {
		// Loop to store and display values of individual checked checkbox.
		foreach ( $_POST ['chk_group'] as $selected ) {
			$templateName = "cmsos-data-" . $_zone . '-' . $_tag . '-' . strtolower ( $selected ) . '-template';
			echo "Deleting template " . $templateName;
			$json = deleteTemplates ( $_POST ['elasticsearchurl'], $templateName );
			if ($json ["acknowledged"]) {
				echo " OK <br>";
			} else {
				print_r ( $json );
			}
			
			$indexNamePattern = "cmsos-data-" . $_zone . '-' . $_tag . '-' . strtolower ( $selected ) . '_*';
			echo "Deleting indices " . $indexNamePattern;
			$json = deleteIndices ( $_POST ['elasticsearchurl'], $indexNamePattern );
			if ($json ["acknowledged"]) {
				echo " OK <br>";
			} else {
				print_r ( $json );
			}
		}
	}
}

echo '<br>';

echo '<form action="types.php">';
echo '<input type="submit" value="Go back" method="get">';
echo '<input type="hidden" name="zone" value="' . $_zone . '"/>';
echo '<input type="hidden" name="tag" value="' . $_tag . '"/>';
echo '</form>'?>

</body>
</html>