<html>

<head>
<meta charset="utf-8">
<title>CMS - Flashweb</title>
<link href="css/flashweb.css" rel="stylesheet" />
</head>

<body>
<?php
include_once ('tools.php');
$_zone = $_POST ['zone'];
$_tag = $_POST ['tag'];
if (! empty ( $_POST ['chk_group'] )) {
	// Loop to store and display values of individual checked checkbox.
	foreach ( $_POST ['chk_group'] as $selected ) {
		$templateName = "cmsos-data-" . $_zone . "-" . $_tag . "-" . strtolower ( $selected ) . '-template';
		if (isset ( $_POST ['enable'] )) {
			echo "Enabling rotate for '" . $templateName . "' <br>" . PHP_EOL;
			$json = setRotate ( $_POST ['elasticsearchurl'], $_zone, $_tag, $selected, "true" );
		}
		else {
			echo "Disabling rotate for '" . $templateName . "' <br>" . PHP_EOL;
			$json = setRotate ( $_POST ['elasticsearchurl'], $_zone, $_tag, $selected, "false" );
		}
	}
}

echo '<br>';

echo '<form action="types.php">';
echo '<input type="submit" value="Go back" method="get">';
echo '<input type="hidden" name="zone" value="' . $_zone . '"/>';
echo '<input type="hidden" name="tag" value="' . $_tag . '"/>';
echo '</form>';
?>

</body>
</html>
