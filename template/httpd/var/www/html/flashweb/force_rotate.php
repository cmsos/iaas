<html>

<head>
<meta charset="utf-8">
<title>CMS - Flashweb</title>
<link href="css/flashweb.css" rel="stylesheet" />
</head>

<body>
<?php
include_once ('tools.php');
if (isset ( $_POST ['rotate'] )) { // to run PHP script on submit
	if (! empty ( $_POST ['chk_group'] )) {
		// Loop to store and display values of individual checked checkbox.
		foreach ( $_POST ['chk_group'] as $selected ) {
			indexRotate($_POST ['elasticsearchurl'], $_POST ['signature'], $selected, '*', true);
			aliasRotate($_POST ['elasticsearchurl'], $_POST ['signature'], $selected, '*', true);
			echo "Rotated indices and aliases for the '" . $selected . "' zone" . "<br>" . PHP_EOL;
		}
	}
}

echo '<br>';

echo '<form action="index.php">';
echo '<input type="submit" value="Go back" method="get">';
echo '</form>';
?>

</body>
</html>