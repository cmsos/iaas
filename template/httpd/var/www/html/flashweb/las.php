<?php

function topHits($url, $index, $flash, $maxSources, $filter) {
	$response = topHitsRawData ( $url, $index, $flash, $maxSources, $filter );
	$json = json_decode ( $response, true );
	return $json;
}
function getMapping($url, $zone, $tag, $flash) {
	$templateName = "cmsos-data-" . $zone . "-" . $tag . "-" . strtolower ( $flash ) . "-template";
	$response = \Httpful\Request::get ( $url . "/_template/" . $templateName )->send ();
	// echo '<pre>' . $response . '</pre>';
	$json = json_decode ( $response, true );
	return $json [$templateName] ["mappings"];
}
function allHitsRawData($url, $index, $flash, $maxSources, $filter) {
	$query = '{' . buildFilter ( $filter ) . '}';
	// echo $query;
	$response = \Httpful\Request::post ( $url . "/" . $index . "/" . $flash . "/_search?size=" . $maxSources )->sendsJson ()->body ( $query )->send ();
	
	return $response;
}
function topHitsRawData($url, $index, $flash, $maxSources, $filter) {
	$query = '{' . buildFilter ( $filter ) . ',"size":0,"aggs":{"group_by_context":{"terms":{"field":"flash_key","size": ' . $maxSources . '},"aggs":{"top_metrics":{"top_hits":{ "sort" : [{"creationtime_" : { "order" : "desc"}}], "size":1}}}}}}';
	
	$response = \Httpful\Request::post ( $url . "/" . $index . "/" . $flash . "/_search" )->sendsJson ()->body ( $query )->send ();
	
	return $response;
}

function displayLASJSON($array, $flashtype) {
	$closeformat = '';

	$arrayobject = new ArrayObject ( $array );
	$iterator = $arrayobject->getIterator ();

	if (! ($iterator->valid ())) {
		if ($flashtype == 'simple') {
			echo '"[]"';
		} else {
			echo '"{"rows":0,"cols":0,"definition":[],"data":[]}"';
		}
		return;
	}

	$html = FALSE;
	// store table keys for ordered display
	$header_fields = array ();

	while ( $iterator->valid () ) {

		$val = $iterator->current ();

		if (! is_array ( $val )) {
			if ($iterator->key () == 0) {
				echo '"[';
				$closeformat = ']"';
			}
			if ($iterator->key () > 0) {
				echo ",";
			}
			echo $val;
		} else {
				
			if ($iterator->key () == 0) {
				$closeformat = ']}"';
				// build ordered list of header fields
				foreach ( $val as $key => $field ) {
					array_push ( $header_fields, $key );
				}
				sort ( $header_fields );
				$rows = sizeof ( $arrayobject );
				$cols = sizeof ( $header_fields );

				$html = TRUE;
				echo '"{';

				echo '"rows": ' . $rows . ',';
				echo '"cols": ' . $cols . ',';

				echo '"definition": [';

				reset ( $header_fields );
				$first = key ( $header_fields );
				foreach ( $header_fields as $key => $name ) {
					if ($key !== $first) {
						echo ',';
					}
					echo '["' . $name . '","unknown"]';
				}

				echo '],';
				echo '"data": [';
			}
				
			if ($iterator->key () > 0) {
				echo ",";
			}
			echo '[';
				
			reset ( $header_fields );
			$first = key ( $header_fields );
			foreach ( $header_fields as $key => $name ) {
				if ($key !== $first) {
					echo ',';
				}

				$typename = gettype ( $val [$name] );
				// echo '>'.$mytypename.'<';

				if ($typename == "string") {
					echo '"';
				}

				if (is_array ( $val [$name] )) {
					displayLASJSON ( $val [$name], 'simple' );
				} else {
					echo $val [$name];
				}

				if ($typename == "string") {
					echo '"';
				}
			}
			echo ']';
		}

		$iterator->next ();
	}
	echo $closeformat;
}

function retrieveTopHitsCSV($url, $zone, $tag, $index, $flash, $maxSources, $delimiter_, $filter) {
	$json = topHits ( $url, $index, $flash, $maxSources, $filter );
	if ( isErrorResponse($json) )
	{
		throw new Exception ( $json['error']['reason'],  $json['status']);
	}
	$mapping = getMapping ( $url, $zone, $tag, $flash );

	$last_key = "";
	$header_fields = array ();
	foreach ( $json ['aggregations'] ['group_by_context'] ['buckets'] as $bucket ) {
		foreach ( $bucket ['top_metrics'] ['hits'] ['hits'] as $hits ) {
			foreach ( $hits ['_source'] as $key => $field ) {
				array_push ( $header_fields, $key );
			}
		}
		sort ( $header_fields );
		$last_key = end ( $header_fields );
		
		break;
	}
	
	// Output table in html
	foreach ( $header_fields as $name ) {
		
		echo $name;
		if ($name != $last_key) {
			echo $delimiter_;
		}
	}
	
	echo "\r\n";
	$notFirst = false;
	
	foreach ( $json ['aggregations'] ['group_by_context'] ['buckets'] as $bucket ) {
		
		if ($notFirst) {
			echo "\r\n";
		}
		foreach ( $bucket ['top_metrics'] ['hits'] ['hits'] as $hits ) {
			
			foreach ( $header_fields as $name ) {
				$field = $hits ['_source'] [$name];
				
				if (is_array ( $field )) {
					$flashtype = getFlashType ( $mapping, $flash, $name );
					displayLASJSON ( $field, $flashtype );
					// displayCSV($field, $delimiter_);
				} else if (getFieldType ( $mapping, $flash, $name ) == "date") {
					if (strpos ( $field, '.' ) !== FALSE) {
						echo '"' . $field . '"';
					} else {
						$newtime = str_replace ( "Z", ".000000Z", $field );
						echo '"' . $newtime . '"';
					}
				} else if (getFieldType ( $mapping, $flash, $name ) == "boolean") {
					if ($field == 1) {
						echo '"true"';
					} else {
						echo '"false"';
					}
				} else {
					echo '"' . $field . '"';
				}
				
				if ($name != $last_key) {
					echo $delimiter_;
				}
			}
		}
		
		$notFirst = true;
		// echo "\r\n";
	}
}
function retrieveCSV($url, $zone, $tag, $index, $flash, $maxSources, $delimiter_, $filter) {
	$json = allHits ( $url, $index, $flash, $maxSources, $filter );
	$mapping = getMapping ( $url, $zone, $tag, $flash );
	
	$last_key = "";
	$header_fields = array ();
	// extract table header fields
	foreach ( $json ['hits'] ['hits'] as $val ) {
		
		foreach ( $val ['_source'] as $key => $field ) {
			array_push ( $header_fields, $key );
		}
		sort ( $header_fields );
		$last_key = end ( $header_fields );
		break;
	}
	
	foreach ( $header_fields as $name ) {
		echo $name;
		if ($name != $last_key) {
			echo $delimiter_;
		}
	}
	// end of header fields
	
	echo "\r\n";
	
	$notFirst = false;
	
	foreach ( $json ['hits'] ['hits'] as $val ) {
		if ($notFirst) {
			echo "\r\n";
		}
		foreach ( $header_fields as $name ) {
			$field = $val ['_source'] [$name];
			
			if (is_array ( $field )) {
				displayCSV ( $field, $delimiter_ );
			} else if (getFieldType ( $mapping, $flash, $name ) == "date") {
				if (strpos ( $field, '.' ) !== FALSE) {
					echo '"' . $field . '"';
				} else {
					$newtime = str_replace ( "Z", ".000000Z", $field );
					echo '"' . $newtime . '"';
				}
			} else if (getFieldType ( $mapping, $flash, $name ) == "boolean") {
				if ($field == 1) {
					echo '"true"';
				} else {
					echo '"false"';
				}
			} else {
				echo '"' . $field . '"';
			}
			
			if ($name != $last_key) {
				echo $delimiter_;
			}
		}
		$notFirst = true;
		// echo "\r\n";
	}
}
function retrieveTopHitsHTML($url, $index, $flash, $maxSources, $filter) {
	$json = topHits ( $url, $index, $flash, $maxSources, $filter );
	if ( isErrorResponse($json) )
	{
		throw new Exception ($json['error']['reason'], $json['status']);
	}
	
	if ( isset($json["status"]) && ($json["status"] != 200) ) {
		header ( "Location: " . $index . "/" . $flash, false, 404 );
		return;
	}
	
	echo '<!doctype html>';
	echo '<html>';
	echo '<head>';
	echo '<meta charset="utf-8">';
	echo '<title>CMS - Flashweb</title>';
	echo '<link href="css/flashweb.css" rel="stylesheet" />';
	echo '</head>';
	echo '<body>';
	
	echo '<br>';
	
	$header_fields = array ();
	if (isset ( $json ['aggregations'] ['group_by_context'] ['buckets'] )) {
		// extract table header fields
		foreach ( $json ['aggregations'] ['group_by_context'] ['buckets'] as $bucket ) {
			foreach ( $bucket ['top_metrics'] ['hits'] ['hits'] as $hits ) {
				foreach ( $hits ['_source'] as $key => $field ) {
					array_push ( $header_fields, $key );
				}
			}
			sort ( $header_fields );
			break;
		}
	}
	
	// Output table in html
	echo '<table class="xdaq-table">';
	
	echo '<thead>';
	foreach ( $header_fields as $name ) {
		
		echo '<th>';
		echo $name;
		echo '</th>';
	}
	echo '</thead>';
	
	echo '<tbody>';
	if (isset ( $json ['aggregations'] ['group_by_context'] ['buckets'] )) {
		foreach ( $json ['aggregations'] ['group_by_context'] ['buckets'] as $bucket ) {
			echo "<tr>";
			foreach ( $bucket ['top_metrics'] ['hits'] ['hits'] as $hits ) {
				
				foreach ( $header_fields as $name ) {
					$field = $hits ['_source'] [$name];
					
					echo '<td>';
					if (is_array ( $field )) {
						
						displayHTML ( $field );
					} else {
						echo $field;
					}
					
					echo '</td>';
				}
			}
			echo "</tr>";
		}
	} else {
		print_r ( $json );
	}
	echo '</tbody>';
	echo '</table>';
	echo '</body>';
	echo '</html>';
}
function retrieveHTML($url, $index, $flash, $maxSources, $filter) {
	$json = allHits ( $url, $index, $flash, $maxSources, $filter );
	
	echo '<br>';
	
	$header_fields = array ();
	// extract table header fields
	foreach ( $json ['hits'] ['hits'] as $val ) {
		foreach ( $val ['_source'] as $key => $field ) {
			array_push ( $header_fields, $key );
		}
		sort ( $header_fields );
		break;
	}
	
	// Output table in html
	echo '<table class="xdaq-table">';
	
	echo '<thead>';
	foreach ( $header_fields as $name ) {
		
		echo '<th>';
		echo $name;
		echo '</th>';
	}
	echo '</thead>';
	
	echo '<tbody>';
	
	foreach ( $json ['hits'] ['hits'] as $val ) {
		echo "<tr>";
		
		foreach ( $header_fields as $name ) {
			$field = $val ['_source'] [$name];
			echo '<td>';
			
			if (is_array ( $field )) {
				displayHTML ( $field );
			} else {
				echo $field;
			}
			
			echo '</td>';
		}
		echo "</tr>";
	}
	
	echo '</tbody>';
	echo '</table>';
}
function retrieveTopHitsJSON($url, $index, $flash, $maxSources, $indent, $filter) {
	$response = topHitsRawData ( $url, $index, $flash, $maxSources, $filter );
	if ( isErrorResponse($response) )
	{
		throw new Exception ( $response['error']['reason'],  $response['status']);
	}
	if ($indent) {
		echo indent ( $response );
	} else {
		echo $response;
	}
}
function retrieveJSON($url, $index, $flash, $maxSources, $indent, $filter) {
	$response = allHitsRawData ( $url, $index, $flash, $maxSources, $filter );
	
	if ($indent) {
		echo indent ( $response );
	} else {
		echo $response;
	}
}
function displayHTML($array) {
	$arrayobject = new ArrayObject ( $array );
	$iterator = $arrayobject->getIterator ();
	
	$html = FALSE;
	// store table keys for ordered display
	$header_fields = array ();
	
	while ( $iterator->valid () ) {
		
		$val = $iterator->current ();
		
		if (! is_array ( $val )) {
			if ($iterator->key () == 0) {
				// echo '"[';
			}
			if ($iterator->key () > 0) {
				echo ",";
			}
			echo $val;
		} else {
			
			if ($iterator->key () == 0) {
				// build ordered list of header fields
				foreach ( $val as $key => $field ) {
					array_push ( $header_fields, $key );
				}
				sort ( $header_fields );
				
				$html = TRUE;
				echo '<table class="xdaq-table">';
				
				echo '<thead>';
				// foreach ($val as $key => $field) {
				foreach ( $header_fields as $name ) {
					
					echo '<th>';
					echo $name;
					echo '</th>';
				}
				
				echo '</thead>';
				echo '<tbody>';
			}
			
			echo "<tr>";
			// foreach ($val as $field) {
			foreach ( $header_fields as $name ) {
				echo '<td>';
				
				if (is_array ( $val [$name] )) {
					displayHTML ( $val [$name] );
				} else {
					echo $val [$name];
				}
				
				echo '</td>';
			}
			echo "</tr>";
		}
		
		$iterator->next ();
	}
	if ($html) {
		echo '</tbody>';
		echo '</table>';
	}
}
function allHits($url, $index, $flash, $maxSources, $filter) {
	$query = '{' . buildFilter ( $filter ) . '}';
	// echo $query;
	$response = \Httpful\Request::post ( $url . "/" . $index . "/" . $flash . "/_search?size=" . $maxSources )->sendsJson ()->body ( $query )->send ();
	
	$json = json_decode ( $response, true );
	
	return $json;
}

function retrieveCatalogJSON($url, $zone) {
	// perform request to ES
	$response = \Httpful\Request::get ( $url . "/_template/" . $zone ."-*?pretty" )->send ();
	return $response;
}
function retrieveCatalogHTML($url, $zone) {
	global $config;
	echo '<table id = "summary-table" class="xdaq-table">';
	echo '<thead>';
	echo '<tr>';
	echo '<th>Name</th>';
	echo '</tr>';
	echo '</thead>';
	
	echo '<tbody>';
	$templates = retrieveTemplates ( $url, $zone . '-*' );
	foreach ( $templates as $key => $val ) {
		$mappings = $val ["mappings"];
		$flashlist = key ( $mappings );
		if (validateSignature ( $flashlist, $mappings, $config ['timestreamsignature'] )) {
			echo '<tr><td>';
			echo "urn:xdaq-flashlist:" . $flashlist;
			echo '</tr></td>';
		}
	}
	
	echo '</tbody>';
	echo '</table>';
}
function retrieveCatalogCSV($url, $zone) {
	global $config;
	echo "Name\r\n";
	$templates = retrieveTemplates ( $url, $zone . '-*' );
	foreach ( $templates as $key => $val ) {
		$mappings = $val ["mappings"];
		$flashlist = key ( $mappings );
		if (validateSignature ( $flashlist, $mappings, $config ['timestreamsignature'] )) {
			echo "urn:xdaq-flashlist:" . $flashlist;
			echo "\r\n";
		}
	}
}

?>
