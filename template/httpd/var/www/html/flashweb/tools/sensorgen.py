#!/usr/bin/python3

import sys
import os
import io
import json
from xml.dom import Node, minidom

if sys.version_info[0] != 3:
	raise Exception("Python 3 is required!")

parameterCount = len(sys.argv)
if parameterCount != 5:
	print("Usage python3 sensorgen.py <zone> <tag> <inputDir> <outputDir>")
	exit()

zone = sys.argv[1]
tag = sys.argv[2]
inputDir = sys.argv[3]
outputDir = sys.argv[4]

print("zone = '{0}', tag = '{1}', inputDir = '{2}', outputDir = '{3}'".format(zone, tag, inputDir, outputDir))

#
# convert original XML flashlist in json format (in elasticsearch _meta )
#
def generateJSONSensor(zone, tag, name,  domFlashlist):
	root = domFlashlist.documentElement;
	monitors = []
	for monitor in root.childNodes:
		if (monitor.nodeType == Node.ELEMENT_NODE) and (monitor.localName == "monitor"):
			m = {}
			samplers = []
			for item in monitor.childNodes:
				if (item.nodeType == Node.ELEMENT_NODE) and (item.localName == "flash"):
					attribValue = item.getAttributeNS("http://www.w3.org/1999/xlink", "href")
					m["name"] = attribValue.split("#")[1]
				elif (item.nodeType == Node.ELEMENT_NODE) and (item.localName == "sampler"):
					s = {}
					for attribName in item.attributes.keys():
						attribValue = item.getAttribute(attribName)
						s[attribName] = attribValue
					samplers.append(s)
			m["samplers"] = samplers
			monitors.append(m)
	json = {"monitors": monitors, "service": name}
	return json

def crawlDirectories():
	for filename in os.listdir(inputDir):
		if filename.endswith(".sensor"):
			name = os.path.splitext(filename)[0]
			filePath = os.path.join(inputDir, filename)
			domFlashlist = minidom.parse(filePath)
			#print(filePath)
			#with open(filePath, 'r') as fin:
			#	print(fin.read())
			document = generateJSONSensor(zone, tag, name, domFlashlist)
			outputfilepath = os.path.join(outputDir, name + ".json")
			with open(outputfilepath, 'w') as outfile:
				json.dump(document, outfile, indent = 3, separators = (',', ': '))
			print("Generated file: {0}".format(outputfilepath))
			#print(json.dumps(document, indent = 3, separators = (',', ': ')))

crawlDirectories()

