#!/usr/bin/python3

import time
import getopt, sys
import http.client
import ssl
ssl._create_default_https_context = ssl._create_unverified_context
import subprocess

#Args used to parameterize 
zone='bril'
host='unknown'
port=9200
version='master'
settings='/opt/xdaq/share/'
gendir='/tmp'
numberofreplicas=1


full_cmd_arguments = sys.argv
argument_list = full_cmd_arguments[1:]
short_options = "hz:e:p:v:s:r:"
long_options = ["help", "zone=", "elasticsearchhost=", "port=", "settingsdir=", "version=", "numberofreplicas="]

try:
    arguments, values = getopt.getopt(argument_list, short_options, long_options)
except getopt.error as err:
    # Output error, and return with an error code
    print (str(err))
    sys.exit(2)

for current_argument, current_value in arguments:
    if current_argument in ("-h", "--help"):
        print ("Usage: metasphere.py -h,--help, -z,--zone=<name> -e,--elasticsearchhost=<hostname> -p,--port=# -v,--version=<version> -s,--settingsdir=<path>")
        sys.exit(0)
    elif current_argument in ("-z", "--zone"):
        zone = current_value
    elif current_argument in ("-e", "--elasticsearchhost"):
        host = current_value
    elif current_argument in ("-p", "--port"):
        port = int(current_value)
    elif current_argument in ("-s", "--settings"):
        settings = current_value
    elif current_argument in ("-v", "--version"):
        version = current_value
    elif current_argument in ("-r", "--numberofreplicas"):
        numberofreplicas = int(current_value)
    else:
        print ("Invalid option")
        sys.exit(2)

print(zone)
print(host)
print(port)
print(version)
print(settings)

gendir = subprocess.check_output(["mktemp", "-d", "/tmp/metasphere.XXXXXXXXX"])
gendir = gendir.decode('utf-8').strip()
print("gendir = " + gendir)

subprocess.call(["mkdir", "-p", gendir + "/flashlists.outdir"])
subprocess.call(["mkdir", "-p", gendir + "/collector.outdir"])
subprocess.call(["mkdir", "-p", gendir + "/sensors.outdir"])
subprocess.call(["mkdir", "-p", gendir + "/pulsers.outdir"])
subprocess.call(["mkdir", "-p", gendir + "/slashes.outdir"])

subprocess.call(["/var/www/html/flashweb/tools/flashlistgen.py", zone, version, settings + "/" + zone + "/flash", gendir + "/flashlists.outdir"])
subprocess.call(["/var/www/html/flashweb/tools/collectgen.py", zone, version, settings + "/" + zone + "/flash", gendir + "/collector.outdir"])
subprocess.call(["/var/www/html/flashweb/tools/sensorgen.py", zone, version, settings + "/" + zone + "/sensor", gendir + "/sensors.outdir"])
subprocess.call(["/var/www/html/flashweb/tools/pulsergen.py", zone, version, settings + "/" + zone + "/sensor", gendir + "/pulsers.outdir"])
subprocess.call(["/var/www/html/flashweb/tools/slashgen.py", zone, version, settings + "/" + zone + "/sensor", gendir + "/slashes.outdir"])

subprocess.call(["/var/www/html/flashweb/tools/deploy.py", zone, version, "flashlists", host + ":" + str(port), gendir + "/flashlists.outdir", "id", str(numberofreplicas)])
subprocess.call(["/var/www/html/flashweb/tools/deploy.py", zone, version, "collector", host + ":" + str(port), gendir + "/collector.outdir", "name", str(numberofreplicas)])
subprocess.call(["/var/www/html/flashweb/tools/deploy.py", zone, version, "sensors", host + ":" + str(port), gendir + "/sensors.outdir", "service", str(numberofreplicas)])
subprocess.call(["/var/www/html/flashweb/tools/deploy.py", zone, version, "pulsers", host + ":" + str(port), gendir + "/pulsers.outdir", "service", str(numberofreplicas)])
subprocess.call(["/var/www/html/flashweb/tools/deploy.py", zone, version, "slashes", host + ":" + str(port), gendir + "/slashes.outdir", "slashInstance", str(numberofreplicas)])

# major number at 2nd position because of string 'release'
arr = version.split("_")
major = arr[1]
subprocess.call(["/var/www/html/flashweb/tools/autotagging.py", host + ":" + str(port), zone, version, "staging_" + major, str(numberofreplicas)])
subprocess.call(["/var/www/html/flashweb/tools/autotagging.py", host + ":" + str(port), zone, version, "production_" + major, str(numberofreplicas)])

subprocess.call(["rm", "-rf", gendir])
