#!/usr/bin/python3

import sys
import os
import io
import json
from xml.dom import Node, minidom

if sys.version_info[0] != 3:
	raise Exception("Python 3 is required!")

parameterCount = len(sys.argv)
if parameterCount != 5:
	print("Usage python3 flashlistgen.py <zone> <tag> <inputDir> <outputDir>")
	exit()

zone = sys.argv[1]
tag = sys.argv[2]
inputDir = sys.argv[3]
outputDir = sys.argv[4]

print("zone = '{0}', tag = '{1}', inputDir = '{2}', outputDir = '{3}'".format(zone, tag, inputDir, outputDir))

#
# convert original XML flashlist in json format (in elasticsearch _meta )
#
def generateJSONDefinition(items):
	definition = {}
	for item in items:
		if (item.nodeType == Node.ELEMENT_NODE) and (item.nodeName == "xmas:item"):
			#print("item.nodeName = {0}".format(item.nodeName))
			definition[item.attributes["name"].nodeValue] = {}
			property = definition[item.attributes["name"].nodeValue]
			for attribName in item.attributes.keys():
				attribValue = item.getAttribute(attribName)
				property[attribName] = attribValue
				#print("{0}: {1}".format(attribName, attribValue))
				attributeNode = item.getAttributeNode(attribName)
				if attributeNode.prefix != None:
					property["xmlns:" + attributeNode.prefix] = attributeNode.namespaceURI
				
			if len(item.childNodes) > 0:
				property["definition"] = generateJSONDefinition(item.childNodes)
	return definition

def generateJSONFlashlist(zone, tag, domFlashlist):
	root = domFlashlist.documentElement;

	id = root.getAttribute("id")
	version = root.getAttribute("version")
	key = root.getAttribute("key")

	json = {}
	json["id"] = id
	json["version"] = version
	json["key"] = key
	json["signature"] = "urn:cmsos:xdaq-timestream"
	json["zone"] = zone
	json["tag"] = tag
	json["definition"] = generateJSONDefinition(root.childNodes)

	return json

def crawlDirectories():
	for filename in os.listdir(inputDir):
		if filename.endswith(".flash"):
			name = os.path.splitext(filename)[0]
			filePath = os.path.join(inputDir, filename)
			domFlashlist = minidom.parse(filePath)
			#print(filePath)
			#with open(filePath, 'r') as fin:
			#	print(fin.read())
			document = generateJSONFlashlist(zone, tag, domFlashlist)
			outputfilepath = os.path.join(outputDir, name + ".json")
			with open(outputfilepath, 'w') as outfile:
				json.dump(document, outfile, indent = 3, separators = (',', ': '))
			print("Generated file: {0}".format(outputfilepath))
			#print(json.dumps(document, indent = 3, separators = (',', ': ')))

crawlDirectories()

