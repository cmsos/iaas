<?php
include_once ('tools.php');
include_once ('config.php');
include_once ('las.php');

ini_set ( 'display_errors', 'On' );
error_reporting ( E_ALL | E_STRICT );
// get args
$fmt_ = $_GET ["fmt"];
$zone_ = $_GET ["zone"];

// retrieve full set of flashlists
// $es_index_ = "flashlist";

// build url
// $url_ = 'http://' . $config['host']. ":" . $config['flash_index']. "/" . $flash_ . "/_search?";

if ($fmt_ == "csv") {
	header ( "Pragma: public" ); // required
	header ( "Expires: 0" );
	header ( "Cache-Control: must-revalidate, post-check=0, pre-check=0" );
	header ( "Cache-Control: private", false ); // required for certain browsers
	header ( "Content-type: application/csv" ); // I always use this
	header ( "Content-Disposition: attachment; filename=catalog.csv" );
	header ( "Content-Transfer-Encoding: binary" );
	
	// header('Content-type: application/csv');
	// header('Content-Disposition: attachment; filename=catalog.csv');
	// header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
	
	retrieveCatalogCSV ( $config ['elasticsearchurl'], $zone_ );
} else if ($fmt_ == "html") {
	echo '<!doctype html>';
	echo '<html>';
	echo '<head>';
	echo '<meta charset="utf-8">';
	echo '<title>CMS - Escaped (Elasticsearch capability for enhanced data aquisition)</title>';
	echo '<link href="css/tables.css" rel="stylesheet" />';
	echo '</head>';
	echo '<body>';
	
	retrieveCatalogHTML ( $config ['elasticsearchurl'], $zone_ );
	echo '</body>';
	echo '</html>';
} else if ($fmt_ == "json") {
	
	header ( 'Content-type: application/json' );
	header ( 'Content-Disposition: attachment; filename=catalog.json' );
	header ( "Cache-Control: no-cache, must-revalidate" ); // HTTP/1.1
	
	echo retrieveCatalogJSON ( $config ['elasticsearchurl'], $zone_ );
} else if ($fmt_ == "plain") {
	header ( 'Content-type: text/plain' );
	
	retrieveCatalogCSV ( $config ['elasticsearchurl'], $zone_ );
} else {
	header ( "HTTP/1.1 404 Not Found" );
}
?>
