# $Id$

#########################################################################
# XDAQ Components for Distributed Data Acquisition                      #
# Copyright (C) 2000-2019, CERN.                                        #
# All rights reserved.                                                  #
# Authors: L. Orsini and D. Simelevicius                                #
#                                                                       #
# For the licensing terms see LICENSE.                                  #
# For the list of contributors see CREDITS.                             #
#########################################################################

##
#
# Project level Makefile
#
##

BUILD_HOME:=$(shell pwd)

ifndef PACKAGES
PACKAGES= \
   cms/service/elasticsearch \
   cms/service/httpd \
   cms/service/rotate \
   bril/service/elasticsearch \
   bril/service/httpd \
   bril/service/rotate
endif
export PACKAGES

BUILD_SUPPORT=build
export BUILD_SUPPORT

PROJECT_NAME=iaas
export PROJECT_NAME

include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfAutoconf.rules

Project=$(PROJECT_NAME)

include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfDefs.$(XDAQ_OS)

Packages=$(PACKAGES)

include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/Makefile.rules
include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfRPM.rules

