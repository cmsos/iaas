#########################################################################
# XDAQ Components for Distributed Data Acquisition                      #
# Copyright (C) 2000-2021, CERN.                                        #
# All rights reserved.                                                  #
# Authors: L. Orsini and D. Simelevicius                                #
#                                                                       #
# For the licensing terms see LICENSE.                                  #
# For the list of contributors see CREDITS.                             #
#########################################################################

#
# Packages to be built
#
BUILD_HOME:=$(shell pwd)/../../..

BUILD_SUPPORT=build
PROJECT_NAME=iaas
include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfAutoconf.rules
include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfDefs.$(XDAQ_OS)

Project=$(PROJECT_NAME)
ZoneName=$(shell pwd | awk -F"/" '{split($$0,a,"/");  print a[NF-2]}')
ServiceName=$(shell pwd | awk -F"/" '{split($$0,a,"/");  print a[NF]}')
Package=$(ZoneName)/service/$(ServiceName)

ifndef BUILD_VERSION
BUILD_VERSION=1
endif

PACKAGE_VER_MAJOR=2
PACKAGE_VER_MINOR=1
PACKAGE_VER_PATCH=0

TEMPLATEDIR=$(BUILD_HOME)/template

build: _buildall

_buildall: all

_all: all

default: all

all:
	cd $(TEMPLATEDIR)/$(ServiceName)/var/www/html/flashweb/; \
	find ./ -name ".svn" -prune -o -type f -exec install -D -m 655 {} $(BUILD_HOME)/$(Package)/var/www/html/flashweb/{} \;
	cp -f $(BUILD_HOME)/$(Package)/flashweb.config.php $(BUILD_HOME)/$(Package)/var/www/html/flashweb/config.php

	cd $(TEMPLATEDIR)/$(ServiceName)/var/www/html/hotspot5/; \
	find ./ -name ".svn" -prune -o -type f -exec install -D -m 655 {} $(BUILD_HOME)/$(Package)/var/www/html/hotspot5/{} \;
	cp -f $(BUILD_HOME)/$(Package)/hotspot5.config.php $(BUILD_HOME)/$(Package)/var/www/html/hotspot5/config.php

	cd $(TEMPLATEDIR)/$(ServiceName)/var/www/html/speedial5/; \
	find ./ -name ".svn" -prune -o -type f -exec install -D -m 655 {} $(BUILD_HOME)/$(Package)/var/www/html/speedial5/{} \;
	cp -f $(BUILD_HOME)/$(Package)/speedial5.config.php $(BUILD_HOME)/$(Package)/var/www/html/speedial5/config.php

	cd $(TEMPLATEDIR)/$(ServiceName)/var/www/html/smarthub5/; \
	find ./ -name ".svn" -prune -o -type f -exec install -D -m 655 {} $(BUILD_HOME)/$(Package)/var/www/html/smarthub5/{} \;
	cp -f $(BUILD_HOME)/$(Package)/smarthub5.config.php $(BUILD_HOME)/$(Package)/var/www/html/smarthub5/config.php

	cd $(TEMPLATEDIR)/$(ServiceName)/var/www/html/timestreamweb/; \
	find ./ -name ".svn" -prune -o -type f -exec install -D -m 655 {} $(BUILD_HOME)/$(Package)/var/www/html/timestreamweb/{} \;
	cp -f $(BUILD_HOME)/$(Package)/timestreamweb.config.php $(BUILD_HOME)/$(Package)/var/www/html/timestreamweb/config.php

	cd $(TEMPLATEDIR)/$(ServiceName)/var/www/html/utils/; \
	find ./ -name ".svn" -prune -o -type f -exec install -D -m 655 {} $(BUILD_HOME)/$(Package)/var/www/html/utils/{} \;

	rm -rf $(BUILD_HOME)/$(Package)/var/www/html/utils/vendor
	tar zxf $(BUILD_HOME)/$(Package)/var/www/html/utils/vendor.tgz -C $(BUILD_HOME)/$(Package)/var/www/html/utils
	
	rm -rf $(BUILD_HOME)/$(Package)/var/www/html/utils/DataTables-1.10.15
	unzip $(BUILD_HOME)/$(Package)/var/www/html/utils/DataTables-1.10.15.zip -d $(BUILD_HOME)/$(Package)/var/www/html/utils
	
	rm -rf $(BUILD_HOME)/$(Package)/var/www/html/utils/Plugins-1.10.15
	tar zxf $(BUILD_HOME)/$(Package)/var/www/html/utils/datatables-plugins-1.10.15.tar.gz -C $(BUILD_HOME)/$(Package)/var/www/html/utils
	
	rm -rf $(BUILD_HOME)/$(Package)/var/www/html/utils/bootstrap-3.3.7-dist
	unzip $(BUILD_HOME)/$(Package)/var/www/html/utils/bootstrap-3.3.7-dist.zip -d $(BUILD_HOME)/$(Package)/var/www/html/utils
	
	rm -rf $(BUILD_HOME)/$(Package)/var/www/html/utils/fancytree-2.24.0
	tar zxf $(BUILD_HOME)/$(Package)/var/www/html/utils/fancytree-2.24.0.tar.gz -C $(BUILD_HOME)/$(Package)/var/www/html/utils

	make -f $(TEMPLATEDIR)/service.makefile ZONE_NAME=$(ZoneName) SERVICE_NAME=$(ServiceName)
_installall: install

install: 

_cleanall: clean

clean:
	rm -rf $(BUILD_HOME)/$(Package)/var
	make -f $(TEMPLATEDIR)/service.makefile ZONE_NAME=$(ZoneName) SERVICE_NAME=$(ServiceName) clean

_rpmall: rpm

rpm:
	make -f $(TEMPLATEDIR)/service.makefile ZONE_NAME=$(ZoneName) SERVICE_NAME=$(ServiceName) BUILD_VERSION=$(BUILD_VERSION) PACKAGE_VER_MAJOR=$(PACKAGE_VER_MAJOR) PACKAGE_VER_MINOR=$(PACKAGE_VER_MINOR) PACKAGE_VER_PATCH=$(PACKAGE_VER_PATCH) rpm


_installrpmall: installrpm

installrpm:
	make -f $(TEMPLATEDIR)/service.makefile ZONE_NAME=$(ZoneName) SERVICE_NAME=$(ServiceName) installrpm

_cleanrpmall: cleanrpm

cleanrpm:
	make -f $(TEMPLATEDIR)/service.makefile ZONE_NAME=$(ZoneName) SERVICE_NAME=$(ServiceName) cleanrpm


